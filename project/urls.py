from django.conf.urls.i18n import i18n_patterns
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings


urlpatterns = i18n_patterns(
    url(r'^i18n/', include('django.conf.urls.i18n')),

    url(r'^admin/', admin.site.urls),
    url(r'^', include('auth2.urls')),
    url(r'^', include('core.urls')),

    url(r'^select2/', include('django_select2.urls')),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),

    prefix_default_language=False
)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
