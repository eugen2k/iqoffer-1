from datetime import timedelta
from django.utils.translation import ugettext_lazy as _
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEBUG = True

CAPTCHA_KEY = '6LejNIIUAAAAALR4cuKwdHJRQjCBUl6fEpY_LoCS'
CAPTCHA_SECRET_KEY = '6LejNIIUAAAAANLkEDUFgZ-Z-HNCW6GXS4jhgd_V'

ALLOWED_HOSTS = ['127.0.0.1']

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'raven.contrib.django.raven_compat',
    'bootstrap4',
    'django_filters',
    'django_extensions',
    'django_select2',
    'ckeditor',
    'debug_toolbar',

    'auth2',
    'core',
    'trans',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'core.middleware.TimeZoneMiddleware',
    'core.middleware.CheckPerformer',
    'core.middleware.CRUDPermission',
    'auth2.middleware.LoginRequired',
    'core.middleware.Perms',
    'core.middleware.SetLastOnlineMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

ROOT_URLCONF = 'project.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'core.context_processors.order_new_messages_count',
                'core.context_processors.service_new_messages_count',
                'core.context_processors.notifications',
                'django.template.context_processors.static',
            ],
            'builtins': [
                'django.templatetags.i18n',
            ],
        },
    },
]

AUTH_USER_MODEL = 'core.User'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

WSGI_APPLICATION = 'project.wsgi.application'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = False

LANGUAGE_CODE = 'en'

LANGUAGES = (
    ('en', 'English'),
    ('ru', 'Русский'),
)

USE_TZ = True

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'static', 'files', 'media')
STATIC_ROOT = os.path.join(BASE_DIR, 'static', 'files', 'static')

DATETIME_INPUT_FORMATS = [
    '%d.%m.%Y %H:%M',
    '%d.%m.%Y',
]
DATETIME_FORMAT = 'd.m.Y H:i'
TIME_FORMAT = 'H:i'
DATE_FORMAT = 'd.m.Y'

REST_FRAMEWORK = dict(
    DEFAULT_AUTHENTICATION_CLASSES=(
        'rest_framework.authentication.SessionAuthentication',
    ),
    DEFAULT_PERMISSION_CLASSES=(
        'rest_framework.permissions.IsAuthenticated',
    ),
)

SWAGGER_SETTINGS = dict(
    SECURITY_DEFINITIONS=dict(
        basic=dict(
            type='basic'
        )
    ),
    USE_SESSION_AUTH=False,
    JSON_EDITOR=True
)

INVITE_SEND_WAITING = timedelta(minutes=1)
INVITE_SUBJECT = 'Invite to iqOffer'
PERFORM_REQUEST = 'Invitation to perform a request'
OFFER_ACCEPT = 'Your offer is the best'
OFFER_DECLINE = 'Offer declined'
ORDER_RESPOND = 'New offer has arrived'
POTENTIAL_REQUEST = 'Potential request'
DEADLINE = 'Your request is expired'
REGISTRATION_ACCEPT = 'Registration accept'
RESET_PASSWORD = 'Password reset'
CUSTOMER_FEEDBACK = 'Customer has rate your company'
PERFORMER_FEEDBACK = 'Provider has rate your company'

SERVER_HOSTNAME = 'https://iqoffer.com'

CKEDITOR_UPLOAD_PATH = 'ckeditor/'
CKEDITOR_IMAGE_BACKEND = "pillow"
