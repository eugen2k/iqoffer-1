#!/usr/bin/python2.7
# -*- coding:utf-8 -*-
import re
import sys

from fabric.main import main

from fabric.api import env, run
from fabric.context_managers import cd

SERVER = 'root@194.177.22.189'
SETTINGS = 'project.settings'

HOME_DIR = '/root/'
PROJECT_DIR = '/root/iqoffer/'

PYTHON_LOCATION = HOME_DIR + 'virtualenv/bin/python '
PIP_LOCATION = HOME_DIR + 'virtualenv/bin/pip '


def prod():
    env.hosts = [SERVER]


def deploy():
    with cd(PROJECT_DIR):
        run('git pull origin master')
        run(PIP_LOCATION + 'install -r requirements.txt')
        run(PIP_LOCATION + 'install --upgrade pip')
        run(PYTHON_LOCATION + 'manage.py createcachetable')
        run(PYTHON_LOCATION + 'manage.py migrate')
        run(PYTHON_LOCATION + 'manage.py collectstatic --noinput')
        run('supervisorctl restart all')


if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw|\.exe)?$', '', sys.argv[0])
    sys.exit(main())
