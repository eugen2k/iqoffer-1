from django.contrib.auth.models import BaseUserManager


class UserManager(BaseUserManager):
    def create_user(self, email, password, **kwargs):
        user = self.model(email=self.normalize_email(email), **kwargs)
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, password, **kwargs):
        defaults = dict(
            email=email,
            password=password,

            is_active=True,
            is_staff=True,
            is_superuser=True
        )
        defaults.update(**kwargs)

        return self.create_user(**defaults)
