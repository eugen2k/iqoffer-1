import datetime

from django import forms
from django.utils.translation import ugettext_lazy as _
from django_select2.forms import Select2Widget
import requests

from core import models
from project.settings import CAPTCHA_SECRET_KEY, CAPTCHA_KEY


class TranslateChoiceField(forms.ModelChoiceField):
    def to_python(self, value):
        if value in self.empty_values:
            return None
        try:
            value = self.model.objects.get(**{'pk': value})
        except (ValueError, TypeError, self.model.DoesNotExist):
            raise forms.ValidationError(self.error_messages['invalid_choice'], code='invalid_choice')
        return value

    def _get_choices(self):
        return self.choices

    def __init__(self, **kwargs):
        self.model = kwargs.pop('model')
        self.choices = kwargs.pop('choices')
        queryset = self.model.objects.none()
        super().__init__(queryset=queryset, **kwargs)


class TranslateMixin:
    _targets = {'country': models.Country, 'city': models.City,
                'subcategory': models.SubCategory, 'category': models.Category,
                'business_category': models.BusinessCategory}

    _labels = {
        'country': _('Country'),
        'city': _('City'),
        'category': _('Category'),
        'subcategory': _('Subcategory'),
        'business_category': _('Business category')
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name in self.fields:
            translate_model = self._targets.get(field_name)
            if translate_model:
                field_choices = []
                required = self.fields[field_name].required
                # if required is False:
                field_choices.append(('', '--------'))

                objects = list(translate_model.objects.all())
                objects.sort(key=lambda obj: (obj.position, _(obj.name)))

                for object in objects:
                    field_choices.append((object.id, _(object.name)))

                self.fields[field_name] = TranslateChoiceField(required=required, model=translate_model,
                                                               choices=field_choices, label=self._labels[field_name])


class Order(TranslateMixin, forms.ModelForm):
    class Meta:
        model = models.Order
        exclude = (
            'owner',
            'offer',
            'winner',
            'published_at',
            'was_published',
            'mailed_at',
        )
        widgets = dict(
            only_for_registered=forms.RadioSelect(
                choices=(
                    (False, _('Visible to everyone')),
                    (True, _(' Visible only to registered users'))
                )
            ),
            description=forms.Textarea(
                attrs=dict(rows=4)
            ),
            category=Select2Widget,
            subcategory=Select2Widget,
        )

    def clean(self):
        cleaned_data = super().clean()
        validation_errors = {}

        if not self.cleaned_data.get('offer_deadline'):
            return cleaned_data

        if datetime.datetime.now().date() > self.cleaned_data['offer_deadline'].date():
            validation_errors['offer_deadline'] = forms.ValidationError(_('Invalid date'))

        if validation_errors:
            raise forms.ValidationError(validation_errors)
        return cleaned_data


class Requisites(TranslateMixin, forms.ModelForm):
    class Meta:
        model = models.Requisites
        exclude = (
            'user', 'pre_user',
        )

    def __init__(self, *args, **kwargs):
        language = kwargs.pop('language')
        super().__init__(**kwargs)
        self.fields['legal_provision'].queryset = models.LegalProvision.objects.filter(language=language)
        self.fields['legal_provision'].label = _('Legal form')
        self.fields['legal_provision'].required = True
        self.fields['company_name'].required = True


class Office(TranslateMixin, forms.ModelForm):
    class Meta:
        model = models.Office
        exclude = (
            'user', 'pre_user',
        )


class DataCenter(TranslateMixin, forms.ModelForm):
    class Meta:
        model = models.DataCenter
        exclude = (
            'user', 'pre_user',
        )


class UserService(TranslateMixin, forms.ModelForm):
    class Meta:
        model = models.UserService
        exclude = (
            'user', 'pre_user',
        )

    def __init__(self, *args, **kwargs):
        self.user = None
        if kwargs.get('user'):
            self.user = kwargs.pop('user')
        super().__init__(*args, **kwargs)
        if self.user:
            if self.user.is_customer:
                self.fields['business_category'].widget.attrs['required'] = True

    def clean(self):
        if self.cleaned_data.get('subcategory'):
            category = self.cleaned_data['category']
            subcategory = self.cleaned_data['subcategory']
            service = self.Meta.model.objects.filter(category=category, subcategory=subcategory, user=self.user).first()
            validator = True

            if self.instance.id:
                validator = self.instance.category != category or self.instance.subcategory != subcategory

            if service and validator:
                raise forms.ValidationError(
                    _('Service with such a bundle category\subcategory already exists: %(service)s') % dict(
                        service=service.name)
                )

        return self.cleaned_data


class UserProfile(TranslateMixin, forms.ModelForm):
    class Meta:
        model = models.User
        fields = (
            'avatar', 'currency', 'username', 'country', 'city', 'business_category', 'timezone',
            'receive_potential_orders',
        )

    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     self.fields['avatar'].widget.attrs['required'] = True


class UserContact(TranslateMixin, forms.ModelForm):
    class Meta:
        model = models.UserContact
        fields = (
            'phone',
            'email',
            'skype',
            'telegram',
            'site',
        )


class Message(TranslateMixin, forms.ModelForm):
    class Meta:
        model = models.Message
        fields = (
            'text', 'service', 'order', 'sender', 'recipient',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['text'].label = False
        self.fields['text'].widget.attrs['placeholder'] = _('Enter message')


class OfferModelForm(TranslateMixin, forms.ModelForm):
    class Meta:
        model = models.Offer
        fields = ('comment', 'amount', 'file',)

        widgets = {
            'comment': forms.Textarea(attrs={'rows': 2})
        }

    def __init__(self, *args, **kwargs):
        order = kwargs.pop('order', None)
        super().__init__(*args, **kwargs)
        self.fields['amount'].help_text = _('Currency:') + ' %s' % order.currency.upper()


class MessageModelForm(TranslateMixin, forms.ModelForm):
    class Meta:
        model = models.Message
        fields = ('text',)

    def __init__(self, *args, **kwargs):
        self.offer = kwargs.pop('offer')
        self.user = kwargs.pop('user')
        self.order = kwargs.pop('order')
        self.messages_history = kwargs.pop('messages_history')
        super().__init__(*args, **kwargs)

    def clean(self):
        cd = super().clean()

        if self.user.is_provider and not self.offer:
            raise forms.ValidationError(_('You can not send a message: the response is not found'))

        return cd


class InviteUserToOrderForm(TranslateMixin, forms.ModelForm):
    order = forms.ModelChoiceField(queryset=None, label=_('My active orders'))

    class Meta:
        model = models.UserServiceInvite
        exclude = ('order', 'user', 'service',)

    def __init__(self, *args, **kwargs):
        orders = kwargs.pop('orders')
        super().__init__(*args, **kwargs)
        self.fields['order'].queryset = orders


class NewForm(TranslateMixin, forms.ModelForm):
    class Meta:
        model = models.New
        exclude = ('created_by',)


class SubCategoryFieldForm(TranslateMixin, forms.Form):
    def __init__(self, field_id=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if field_id:
            field = models.SubCategoryField.objects.get(id=field_id)
            self.fields['sub_field_id'] = forms.IntegerField(initial=field_id, widget=forms.HiddenInput())
        else:
            field = models.SubCategoryField.objects.get(id=kwargs.get('data').get('sub_field_id'))
            self.fields['sub_field_id'] = forms.IntegerField(widget=forms.HiddenInput())

        if field.field_type == models.SubCategoryField.CHAR_FIELD:
            self.fields['value_%s' % field.id] = forms.CharField(label=field.label, required=field.required,
                                                                 help_text=field.help_text)
        if field.field_type == models.SubCategoryField.RANGE_FIELD:
            self.fields['value_%s' % field.id] = forms.IntegerField(
                label=field.label, required=field.required,
                help_text=field.help_text,
                widget=forms.NumberInput(
                    attrs={'type': 'range',
                           'oninput': '%s.value = %s.value' % (
                               'id_value_%s_output' % field.id, 'id_value_%s' % field.id),
                           'step': field.step, 'max': field.max, 'min': field.min, 'class': 'form-control',
                           'value': 0}))
        elif field.field_type == models.SubCategoryField.TEXT_FIELD:
            pass
        elif field.field_type == models.SubCategoryField.INTEGER_FIELD:
            self.fields['value_%s' % field.id] = forms.IntegerField(label=field.label, required=field.required,
                                                                    help_text=field.help_text)
        elif field.field_type == models.SubCategoryField.CHOICE_FIELD:
            select_choices = field.select_choices.split(',')
            field_choices = list(zip(select_choices, select_choices))
            self.fields['value_%s' % field.id] = forms.ChoiceField(label=field.label, choices=field_choices,
                                                                   required=field.required, help_text=field.help_text)
        elif field.field_type == models.SubCategoryField.MULTIPLE_CHOICE_FIELD:
            select_choices = field.select_choices.split(',')
            field_choices = list(zip(select_choices, select_choices))
            self.fields['value_%s' % field.id] = forms.MultipleChoiceField(label=field.label,
                                                                           choices=field_choices,
                                                                           required=field.required,
                                                                           help_text=field.help_text)


class FeedbackForm(TranslateMixin, forms.ModelForm):
    class Meta:
        model = models.Feedback
        exclude = ('created_at', 'order', 'created_by', 'recipient', 'total_stars',)

        labels = {
            'quality': _('Quality of work'),
            'time': _('Compliance with deadlines'),
            'communication': _('Communication'),
            'is_recommended': _('I recommend'),
            'text': _('Comment'),
        }

        widgets = {
            'quality': forms.NumberInput(attrs={'hidden': True}),
            'time': forms.NumberInput(attrs={'hidden': True}),
            'communication': forms.NumberInput(attrs={'hidden': True}),
            'text': forms.Textarea(attrs={'rows': 3})
        }


class PreUserInviteForm(forms.ModelForm):
    PERM_CHOICES = [
        ('is_provider', 'IT provider'),
        ('is_customer', 'Customer')
    ]

    perm = forms.ChoiceField(choices=PERM_CHOICES, widget=forms.RadioSelect(), label=_('You want to invite a'))

    class Meta:
        model = models.PreUser
        fields = ('email', 'recipient_name', 'from_email', 'from_username',)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super().__init__(*args, **kwargs)
        for field_name in self.fields.keys():
            self.fields[field_name].required = True
            self.fields[field_name].label = ''
        self.fields['email'].widget.attrs['placeholder'] = _("Recepient's email")
        self.fields['recipient_name'].widget.attrs['placeholder'] = _("Recepient's name")
        self.fields['from_email'].widget.attrs['placeholder'] = _("Your email")
        self.fields['from_username'].widget.attrs['placeholder'] = _("Your name")

    def clean(self):
        cleaned_data = self.cleaned_data

        token = self.request.POST.get('token')
        if not token:
            raise forms.ValidationError(
                _('Captcha token missing. Check google options.'),
                code='invalid',
            )

        url = "https://www.google.com/recaptcha/api/siteverify"
        params = {
            'secret': CAPTCHA_SECRET_KEY,
            'response': token,
            'remoteip': self.get_client_ip()
        }
        verify_rs = requests.get(url, params=params, verify=True)
        verify_rs = verify_rs.json()
        score = verify_rs.get("score")
        # status = verify_rs.get("success", False)
        if not score:
            raise forms.ValidationError(
                _('The invitation has not been sent. It seems to us that you are a bot! '
                  'If you do not agree, then contact us support@iqoffer.com'),
                code='invalid',
            )
        else:
            cleaned_data['captcha_score'] = score

        if models.User.objects.filter(email=cleaned_data.get('email')):
            raise forms.ValidationError(forms.ValidationError(_('Sending invitations failed, please try again. '
                                                                'User with such email has already registered.')))

        return cleaned_data

    def get_client_ip(self):
        x_forwarded_for = self.request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = self.request.META.get('REMOTE_ADDR')
        return ip


class OrderFilterForm(TranslateMixin, forms.ModelForm):
    class Meta:
        model = models.Order
        fields = (
            'category',
            'subcategory',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        self.fields['category'].required = False
        self.fields['category'].choices = [('', '--------')] + self.fields['category'].choices
        self.fields['subcategory'].required = False
        if user.is_authenticated and user.is_provider:
            self.fields['potential_requests'] = forms.BooleanField(label=_('Only potential requests'),
                                                                   required=False)


class ServiceFilterForm(TranslateMixin, forms.ModelForm):
    class Meta:
        model = models.UserService
        fields = (
            'country',
            'city',
            'category',
            'subcategory',
            'business_category',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        self.fields['category'].required = False
        self.fields['category'].choices = [('', '--------')] + self.fields['category'].choices
        self.fields['subcategory'].required = False
        if user.is_authenticated and user.is_customer:
            self.fields['my_business_category'] = forms.BooleanField(label=_('Only for my business category'),
                                                                     required=False)
