var $ = django.jQuery;

$(document).ready(function () {
    var citiesManagerInline = new CitiesManagerInline();
});

function CitiesManagerInline(options) {
    var self = this;
    self.ajaxUrl = '/ajax/get/cities/';
    self.$countries = $('select[id*="country"]');

    self.setCitiesOptions = function (countryId, $city) {
        $.ajax({
            type: 'GET',
            url: self.ajaxUrl,
            data: {'country_id': countryId},
            success: function (data) {
                var cityValue = $city.find('option:selected').val();
                $city.html('');
                $.each(data['cities'], function (i, city) {
                    var option = document.createElement('option');
                    option.value = city['id'];
                    option.innerHTML = city['name'];
                    if (city['id'] === parseInt(cityValue)) {
                        option.selected = true
                    }
                    $city.append(option)
                })
            }
        })
    };


    self.manageCities = function ($country) {
        var $city = $country.closest('td').next().find('select');
        var countryId = $country.find('option:selected').val();
        self.setCitiesOptions(countryId, $city)
    };


    self.$countries.each(function () {
        self.manageCities($(this));
        $(this).change(function () {
            self.manageCities($(this))
        })
    });

    $(document).on('click', '.add-row a', function () {
        var $el = $(this).closest('tbody').find('tr');
        var $country = $el.eq($el.length - 2).find('select[id*="country"]');

        self.manageCities($country);
        $country.change(function () {
            self.manageCities($country)
        })
    });

}

