$(document).ready(function () {
    setSubcategories(category_id);

    $('#id_category').change(function () {
        category_id = $(this).find('option:selected').val();
        $("#sub_category_fields").html('');
        setSubcategories(category_id)
    })
});


function setSubcategories(category_id) {
    $.ajax({
        type: 'GET',
        url: get_subs_url,
        data: {'category_id': category_id || ''},
        success: function (data) {
            var sub_categories = data.sub_categories;
            $("#id_subcategory").html('');
            var empty_option = document.createElement('option');
            empty_option.value = '';
            empty_option.innerHTML = '---------';
            $("#id_subcategory").append(empty_option);
            for (var i in sub_categories) {
                var option = document.createElement('option');
                option.innerHTML = sub_categories[i].name;
                option.value = sub_categories[i].id;
                if (sub_id === sub_categories[i].id) {
                    option.selected = true
                }
                $("#id_subcategory").append(option)
            }
        }
    })
}