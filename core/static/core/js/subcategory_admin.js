var $ = django.jQuery;

$(document).ready(function () {

    $('.field-category').find('select').each(function () {
        manageSubcategories($(this));

        $(this).change(function () {
            manageSubcategories($(this))
        })
    })
});


function manageSubcategories($select) {
    category_id = $select.find('option:selected').val();
    var $service = $select.closest('tr');
    var $sub_select = $service.find('.field-subcategory select');
    setSubcategories(category_id, $sub_select);
}


function setSubcategories(category_id, $sub_select) {
    $.ajax({
        type: 'GET',
        url: '/ajax/get/subcategories/',
        data: {'category_id': category_id},
        success: function (data) {
            sub_categories = data.sub_categories;
            var cleared_val = clearSubcategory($sub_select);
            for (var i in sub_categories) {
                var option = document.createElement('option');
                option.innerHTML = sub_categories[i].name;
                option.value = sub_categories[i].id;
                if (cleared_val === sub_categories[i].id) {
                    option.selected = true
                }
                $sub_select.append(option)
            }
        }
    })
}

function clearSubcategories() {
    $('.field-subcategory').find('select').each(function () {
        clearSubcategory($(this))
    });
}

function clearSubcategory($sub_select) {
    var cleared_val = $sub_select.find('option:selected').val();
    $sub_select.html('');
    $sub_select.parent().css('min-width', '150px');
    $sub_select.css('width', '100%');
    var empty_option = document.createElement('option');
    empty_option.value = '';
    empty_option.innerHTML = '---------';
    empty_option.selected = true;
    $sub_select.append(empty_option);
    return parseInt(cleared_val)
}