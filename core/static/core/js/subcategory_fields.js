$(document).ready(function () {
    if (sub_id)
        getFields(sub_id);
   /* var $subcategory = $('#id_subcategory');

    $subcategory.change(function () {
        sub_id = $(this).find('option:selected').val();
        getFields(sub_id);
    });*/
});

function getFields(sub_id) {
    if (request_post) {
        var data = {'post_data': JSON.stringify(request_post_data)}
    }
    else {
        data = {'sub_id': sub_id, 'order_id': order_id}
    }
    $.ajax({
        type: 'GET',
        data: data,
        url: get_fields_url,
        success: function (data) {
            var $fields = $("#sub_category_fields");
            $fields.html('');
            var fields = data['fields'];
            $.each(fields, function (i, field) {
                $fields.append(field)
            });
        }
    })
}