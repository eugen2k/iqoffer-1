$(document).ready(function () {
    $('#id_category, #id_subcategory').mousedown(function (event) {
        event.preventDefault();
        categoriesSelector.getSelector()
    });
});


function CategoriesSelector(options) {
    var self = this;

    self.$modal = $('#category_subcategory_modal');

    self.url = options['url'];

    self.getSelector = function () {
        $.ajax({
            type: 'GET',
            url: self.url,
            success: function (data) {
                self.$modal.find('.modal-body').html(data['template']);
                self.$modal.find('.modal-title').html(data['title']);

                self.$categories = self.$modal.find('.category');
                self.$subcategories = self.$modal.find('.subcategory');
                self.selectedCategory = $('#id_category').val();
                self.selectedSubcategory = $('#id_subcategory').val();
                self.initSelectedCategoryAndSubcategory();
                self.initSelect();
                self.initSave();

                self.$modal.modal()
            }
        })
    };

    self.initSave = function () {
        self.$modal.find('#save').click(function () {
            $('#id_category').val(self.selectedCategory);
            $('#id_subcategory').val(self.selectedSubcategory);

            try {
                getFields(self.selectedSubcategory);
            }
            catch (e) {
            }
            self.$modal.modal('hide')
        });
    };

    self.initSelectedCategoryAndSubcategory = function () {
        if (self.selectedCategory) {
            var $category = $('.category[id=category' + self.selectedCategory + ']');
            $category.addClass('active');
            $('.subcategory' + '.' + self.selectedCategory).show();

            if (self.selectedSubcategory) {
                $('.subcategory[id=subcategory' + self.selectedSubcategory + ']').addClass('active');
            }
        }
    };

    self.initSelect = function () {
        self.$categories.hover(function () {
            self.$subcategories.removeClass('active');
            self.selectedSubcategory = '';

            /*if ($(this).hasClass('active')) { // For click
                $(this).removeClass('active');
                self.$subcategories.hide();
                self.selectedCategory = '';
            }
            else {*/
            self.$categories.removeClass('active');
            $(this).addClass('active');
            self.selectedCategory = $(this).prop('id').replace('category', '');
            /*}*/

            self.$subcategories.each(function () {
                if (!$(this).hasClass('none-subcategory')) {
                    if ($(this).hasClass(self.selectedCategory)) {
                        $(this).show()
                    }
                    else {
                        $(this).hide()
                    }
                }
            })
        });

        self.$subcategories.click(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                self.selectedSubcategory = '';
            }
            else {
                self.$subcategories.removeClass('active');
                $(this).addClass('active');
                self.selectedSubcategory = $(this).prop('id').replace('subcategory', '');
            }
        })
    }
}