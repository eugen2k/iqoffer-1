function initDTPicker(options) {
    var dt_options = {
        format: 'd.m.Y H:i'
    };

    $.each(options, function (key, value) {
        dt_options[key] = value
    });


    $.datetimepicker.setLocale(options['languageCode']);

    $('.picker').find('input').datetimepicker(dt_options);
}
