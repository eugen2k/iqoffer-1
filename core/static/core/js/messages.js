function MessagesManager(messages_count, url) {
    var self = this;
    self.count = parseInt(messages_count);
    self.intervalMS = 10000;  // TODO

    self.handleCount = function () {
        $.ajax({
            type: 'GET',
            url: url,
            success: function (data) {
                if (self.count !== data['count'] && data['count'] > 0) {
                    self.reloadMessagePage();
                }
                self.count = data['count'];
                self.setCount()
            }
        })
    };
    self.setCount = function () {
        if (parseInt(self.count) > 0) {
            $('#messages_count').html('(' + self.count + ')')
        }
    };

    self.reloadMessagePage = function () {
        if (location.pathname.indexOf('message') === 1) {
            location.reload();
        }
    }
}