var $ = django.jQuery;

$(document).ready(function () {
    var citiesManager = new CitiesManager()
});


function CitiesManager(options) {
    var self = this;
    self.ajaxUrl = '/ajax/get/cities/';
    self.cityValue = $("#id_city option:selected").val();
    self.$country = $('#id_country');

    self.manageCitiesOptions = function (countryId) {
        $.ajax({
            type: 'GET',
            url: self.ajaxUrl,
            data: {'country_id': countryId},
            success: function (data) {
                $('#id_city').html('');
                $.each(data['cities'], function (i, city) {
                    var option = document.createElement('option');
                    option.value = city['id'];
                    option.innerHTML = city['name'];
                    if (city['id'] === parseInt(self.cityValue)) {
                        option.selected = true
                    }
                    $('#id_city').append(option)
                })
            }
        })
    };

    self.manageCitiesOptions(self.$country.find('option:selected').val());

    self.$country.change(function () {
            self.manageCitiesOptions($(this).find('option:selected').val())
        }
    );
}