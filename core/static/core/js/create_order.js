$(document).ready(function () {
    $('#order_create').click(function (event) {
        var category = $('#id_category option:selected').val();
        var subcategory = $("#id_subcategory option:selected").val();
        if (category && subcategory) {
            window.location = order_create_url + '?category=' + category + '&subcategory=' + subcategory;
        }
        else {
            window.location = order_create_url
        }
        event.preventDefault()
    })
});