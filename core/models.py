import base64
import random
import string
import textwrap

import datetime

import pytz
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from django.conf import settings
from django.core import validators
from django.core.mail import send_mail
from django.db.models import F, Q
from django.template.loader import render_to_string
from django.utils import timezone, translation
from django.urls import reverse_lazy
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin

from core.datatools.uploaders import get_uploader
from core.datatools import crypto
from core import managers
from core.templatetags.core import is_winner
from project.settings import SERVER_HOSTNAME, LANGUAGES
from core import const

from core.utils import test_days_generator


class User(AbstractBaseUser, PermissionsMixin):
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ('username',)

    avatar = models.ImageField(_('Avatar'), null=True, blank=True, upload_to=get_uploader('users', 'avatars'))
    email = models.EmailField(_('E-mail'), unique=True)
    username = models.CharField(_('Username'), max_length=255, unique=True,
                                help_text=_('e.g. nickname or last/first name'))

    phone = models.BigIntegerField(_('Phone number'), help_text=_('Only digits'), default=0)

    is_staff = models.BooleanField(_('staff status'), default=False, help_text=_(
        'Designates whether the user can log into this admin site.'),
                                   )
    is_active = models.BooleanField(_('active'), default=False, help_text=_(
        'Designates whether this user should be treated as active. '
        'Unselect this instead of deleting accounts.'),
                                    )

    is_customer = models.BooleanField(default=False, verbose_name=_('Customer'))
    is_provider = models.BooleanField(default=False, verbose_name=_('Provider'))

    country = models.ForeignKey('Country', verbose_name=_('Default country'), null=True, blank=True,
                                related_name='users', on_delete=models.CASCADE)
    city = models.ForeignKey('City', verbose_name=_('Default city'), null=True, blank=True, related_name='users',
                             on_delete=models.CASCADE)

    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    last_online_at = models.DateTimeField('Last online at', null=True, blank=True, editable=False)

    currency = models.CharField(_('Default currency'), max_length=255, blank=True, null=True,
                                choices=const.CURRENCY_CHOICES)
    business_category = models.ForeignKey('BusinessCategory', verbose_name=_('Default business category'),
                                          blank=True, null=True, on_delete=models.CASCADE)

    selected_lang = models.CharField(choices=LANGUAGES, default=LANGUAGES[0][0], max_length=20)
    timezone = models.CharField(_("Default time zone"), max_length=255, choices=[(tz, tz) for tz in pytz.all_timezones],
                                default='UTC')

    objects = managers.UserManager()

    rating = models.FloatField(default=0, blank=True, null=True)

    receive_potential_orders = models.BooleanField(verbose_name=_('Receive email notifications about new requests'),
                                                   default=True)

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'

    def __str__(self):
        return self.username or self.email

    def get_short_name(self):
        return self.username

    def get_full_name(self):
        return self.get_short_name()

    def email_user(self, subject, message='', from_email=None, **kwargs):
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def get_reviews(self):
        return Review.objects.filter(order__owner=self)

    def set_rating(self):
        received_feedbacks = self.feedbacks_received
        positions = 3
        if not received_feedbacks.count():
            return 0
        stars_count = sum(received_feedbacks.values_list('total_stars', flat=True))
        s = stars_count / received_feedbacks.count() / positions
        self.rating = round(s * 2) / 2
        self.save()

    def can_see_contacts(self, who_wants_to_see):
        return self.id == who_wants_to_see.id or self.is_provider or (
                self.is_customer and is_winner(self.my_orders.all(), who_wants_to_see))

    @property
    def joined_len(self):
        days = (datetime.datetime.now().date() - self.date_joined.date()).days
        months = days // 30.4
        days -= months * 30.4
        years = months // 12
        months -= years * 12

        res = []
        if years:
            res.append(_('years') + ': %s' % round(years))
        if months:
            res.append(_('months') + ': %s' % round(months))
        if days:
            res.append(_('days') + ': %s' % round(days))

        return ', '.join(res)

    @property
    def offers_won(self):
        return Order.objects.filter(offer__user=self).count()

    def get_positive_feedbacks(self):
        return self.feedbacks_received.filter(is_recommended=True)

    def get_negative_feedbacks(self):
        return self.feedbacks_received.filter(is_recommended=False)


class PreUser(models.Model):
    _is_token = None

    user = models.OneToOneField(User, on_delete=models.PROTECT, blank=True, null=True)
    email = models.EmailField(_('email address'))

    from_username = models.CharField(_('from username'), max_length=255, blank=True)
    from_email = models.EmailField(_('from email address'), blank=True)

    username = models.CharField(max_length=255, blank=True)

    recipient_name = models.CharField(max_length=255, blank=True)

    from_site = models.BooleanField(default=False)

    is_customer = models.BooleanField(default=False, verbose_name=_('Customer'))
    is_provider = models.BooleanField(default=False, verbose_name=_('Provider'))

    selected_lang = models.CharField(verbose_name=_('selected lang'), choices=LANGUAGES, default=LANGUAGES[0][0],
                                     max_length=20)

    avatar = models.ImageField(_('Avatar'), null=True, blank=True, upload_to=get_uploader('users', 'avatars'))

    scheduled_send_at = models.DateTimeField(verbose_name=_('email scheduled send at'), blank=True, null=True)
    send_at = models.DateTimeField(verbose_name=_('email sent at'), blank=True, null=True)
    send_res = models.TextField(blank=True)

    token = models.CharField(blank=True, max_length=255)

    description = models.TextField(_('description'), blank=True)

    show_in_stat = models.BooleanField(_('show in stat'), default=True)

    captcha_score = models.FloatField(blank=True, null=True)

    created_at = models.DateTimeField(verbose_name=_('created at'), default=timezone.now)

    class Meta:
        verbose_name = 'PreUser'
        verbose_name_plural = 'PreUsers'

    def __str__(self):
        return self.email

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._is_token = bool(self.token)

    def save(self, *args, **kwargs):
        if not self.token or not self._is_token:
            self.token = ''.join(
                [random.choice(random.choice(string.ascii_letters + string.digits))
                 for _ in range(10)])
        super().save(*args, **kwargs)

    @classmethod
    def get_actual(cls, qs=None, **kwargs):
        kwargs.update(send_at__isnull=True)
        if qs is None:
            qs = cls.objects.all()
        return qs.filter(**kwargs)

    def email_user(self, subject, message='', from_email=None, **kwargs):
        return send_mail(subject, message, from_email, [self.email], **kwargs)


class UserContact(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, verbose_name=_('User'), null=True, blank=True,
                                on_delete=models.CASCADE, related_name='contacts')
    pre_user = models.OneToOneField(PreUser, on_delete=models.CASCADE, blank=True, null=True, related_name='contacts')

    email = models.EmailField(_('email address'), blank=True)
    skype = models.CharField('Skype', max_length=50, blank=True)
    telegram = models.CharField('Telegram', max_length=50, blank=True)
    phone = models.BigIntegerField(_('Phone'), help_text=_('only digits'), null=True, blank=True)
    site = models.CharField(_('WebSite'), max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = 'User contact'
        verbose_name_plural = 'Users contacts'

    def has_any_contact(self):
        return any([self.email, self.skype, self.telegram, self.phone, self.site])


class Requisites(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, verbose_name=_('User'), null=True, blank=True,
                                on_delete=models.CASCADE)
    pre_user = models.OneToOneField(PreUser, blank=True, null=True, on_delete=models.CASCADE)

    company_name = models.CharField(_('Company name'), max_length=150, blank=True)
    legal_provision = models.ForeignKey('LegalProvision', verbose_name=_('Legal provision'), null=True, blank=True,
                                        on_delete=models.CASCADE)

    payment_account = models.CharField(_('Payment account'), max_length=50, blank=True)
    bank_name = models.CharField(_('Bank name'), max_length=50, blank=True)
    swift = models.CharField(_('SWIFT'), max_length=50, blank=True, help_text=_('Code of beneficiary bank'))
    inn = models.CharField(_('ИНН'), max_length=50, blank=True)
    kpp = models.CharField(_('КПП'), max_length=50, blank=True)
    bik = models.CharField(_('БИК'), max_length=50, blank=True)
    okpo = models.CharField(_('ОКПО'), max_length=50, blank=True)

    class Meta:
        verbose_name = 'Requisites'
        verbose_name_plural = 'Requisites'


class LegalProvision(models.Model):
    language = models.CharField(max_length=255, choices=const.LANGUAGE_CHOICES)
    name = models.CharField(_('Name'), max_length=150)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Legal Form'
        verbose_name_plural = 'Legal Forms'


class UserService(models.Model):
    user = models.ForeignKey(User, verbose_name=_('User'), related_name='services',
                             on_delete=models.CASCADE, blank=True, null=True)
    pre_user = models.ForeignKey(PreUser, on_delete=models.CASCADE, related_name='services', blank=True, null=True)
    name = models.CharField(_('Service name'), max_length=150)
    description = models.TextField(_('Description'), max_length=500, blank=True)

    min_cost = models.PositiveIntegerField(_('Minimal cost'))

    category = models.ForeignKey('Category', verbose_name=_('Category'), on_delete=models.CASCADE)
    subcategory = models.ForeignKey('SubCategory', verbose_name=_('Subcategory'), on_delete=models.CASCADE)
    business_category = models.ForeignKey('BusinessCategory', verbose_name=_('Business category'),
                                          on_delete=models.CASCADE, blank=True, null=True)

    country = models.ForeignKey('Country', verbose_name=_('Country'), null=True, blank=True,
                                related_name='user_services', on_delete=models.CASCADE)
    city = models.ForeignKey('City', verbose_name=_('City'), null=True, blank=True, related_name='user_services',
                             on_delete=models.CASCADE)

    currency = models.CharField(_('Currency'), max_length=255, blank=True, null=True, choices=const.CURRENCY_CHOICES)

    image = models.ImageField(upload_to='services/', blank=True, null=True)

    class Meta:
        verbose_name = 'User service'
        verbose_name_plural = 'Users services'
        unique_together = ('user', 'category', 'subcategory',)

    def __str__(self):
        return '{}: {}'.format(self.name, textwrap.shorten(self.description, width=50, placeholder=' ...'))

    @property
    def get_location(self):
        if self.country and self.city:
            return '%s, %s' % (self.country, self.city)
        elif self.country:
            return self.country
        elif self.city:
            return self.city
        return ''

    def get_new_messages(self):
        return Message.objects.filter(service=self, is_read=False, recipient=self.user)


class BusinessCategory(models.Model):
    name = models.CharField(_('Name'), max_length=250)
    description = models.CharField(_('Description'), max_length=500, blank=True)

    position = models.IntegerField(default=5)

    class Meta:
        verbose_name = 'Business category'
        verbose_name_plural = 'Business categories'
        ordering = ['position', 'name']

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(_('Name'), max_length=250)
    description = models.CharField(_('Description'), max_length=500, blank=True)

    position = models.IntegerField(default=5)

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'
        ordering = ['position', 'name']

    def __str__(self):
        return textwrap.shorten(self.name, width=30, placeholder=' ...')


class SubCategory(models.Model):
    category = models.ForeignKey(Category, verbose_name=_('Category'), related_name='subcategories',
                                 on_delete=models.CASCADE)
    name = models.CharField(_('Name'), max_length=250)
    description = models.CharField(_('Description'), max_length=500, blank=True)

    position = models.IntegerField(default=5)

    class Meta:
        verbose_name = 'SubCategory'
        verbose_name_plural = 'SubCategories'
        ordering = ['position', 'name']

    def __str__(self):
        return '{}: {}'.format(self.category, textwrap.shorten(self.name, width=30, placeholder=' ...'))


class Order(models.Model):
    owner = models.ForeignKey(User, related_name='my_orders', on_delete=models.CASCADE)

    title = models.CharField(_('Title'), max_length=150)
    description = models.TextField(_('Description'), max_length=1800)

    category = models.ForeignKey(Category, verbose_name=_('Category'), related_name='orders', on_delete=models.CASCADE)
    subcategory = models.ForeignKey(SubCategory, verbose_name=_('Subcategory'), related_name='orders',
                                    on_delete=models.CASCADE)

    # min_cost = models.PositiveIntegerField('Минимальная стоимость')

    currency = models.CharField(_('Currency'), max_length=255, choices=const.CURRENCY_CHOICES)

    budget_from = models.PositiveIntegerField(_('Budget from'))
    budget_to = models.PositiveIntegerField(_('Budget to'), null=True, blank=True)

    created_at = models.DateTimeField(auto_now=True)
    published_at = models.DateTimeField(_('Published at'), null=True, blank=True, db_index=True)
    offer_deadline = models.DateTimeField(_('Offer deadline'), null=True, blank=True)

    offer = models.OneToOneField('Offer', verbose_name=_('Offer'), related_name='win_order', null=True, blank=True,
                                 on_delete=models.CASCADE)
    winner = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, related_name='win_orders',
                               on_delete=models.CASCADE)

    only_for_registered = models.BooleanField(_('Visible'), default=False)
    show_contacts_only_for_winner = models.BooleanField(_('Show contact info only to request winner'), default=False)

    file = models.FileField(_('File'), upload_to='order/', blank=True, null=True)

    was_published = models.BooleanField(default=0)

    potential_mailed_at = models.DateTimeField(blank=True, null=True)
    deadline_mailed_at = models.DateTimeField(blank=True, null=True)

    potential_for_my_country = models.BooleanField(_('Find IT provider in my country'), default=False)

    test_days = models.CharField(max_length=10, choices=test_days_generator(), blank=True,
                                 verbose_name=_('Free test drive'),
                                 help_text=_(
                                     'Specify the number of days of free testing period. Note, '
                                     'the more free test period the less IT-providers will reply.'))

    class Meta:
        verbose_name = 'Order'
        verbose_name_plural = 'Orders'
        ordering = ('-created_at',)

    def get_absolute_url(self):
        return reverse_lazy('order_detail', kwargs=dict(pk=self.pk))

    @classmethod
    def get_draft_orders(cls):
        return cls.objects.filter(published_at__isnull=True)

    @classmethod
    def get_successful_orders(cls):
        return cls.objects.filter(winner__isnull=False)

    @classmethod
    def get_fail_orders(cls):
        return cls.objects.filter(offer_deadline__isnull=False, offer_deadline__lt=timezone.now())

    @classmethod
    def get_published_orders(cls):
        return cls.objects.filter(published_at__isnull=False, winner__isnull=True, offer_deadline__isnull=True)

    def get_status(self):
        if not self.published_at:
            status = _('Draft')
        elif self.winner_id:
            status = _('Successful')
        elif self.offer_deadline and timezone.now() > self.offer_deadline:
            status = _('Fail')
        else:
            status = _('Published')

        return status

    def get_status_color(self):
        STATUS_COLOR_MAP = {
            'Draft': 'warning',
            'Successful': 'success',
            'Fail': 'danger',
            'Published': 'primary',
            'Черновик': 'warning',
            'Успешный': 'success',
            'Неудача': 'danger',
            'Опубликовано': 'primary',
        }
        status = self.get_status()
        return STATUS_COLOR_MAP.get(status)

    @classmethod
    def get_actual(cls, qs=None, **kwargs):
        kwargs.update(published_at__isnull=False, offer__isnull=True, was_published=True)
        if qs is None:
            qs = cls.objects.all()

        return qs.filter(**kwargs)

    def has_performer_feedback(self):
        return self.feedbacks.filter(recipient=self.offer.user).exists()

    def has_customer_feedback(self):
        return self.feedbacks.filter(recipient=self.owner).exists()

    def send_potential(self, request):
        services = UserService.objects.filter(user__isnull=False, subcategory=self.subcategory)
        if self.potential_for_my_country:
            services = services.filter(user__country=self.owner.country)

        if self.potential_mailed_at or self.get_status() == 'Draft' or not services:
            return None

        self.potential_mailed_at = datetime.datetime.now()
        self.save()

        for service in services:
            if not service.user.receive_potential_orders:
                continue
            potential_template = render_to_string('core/emails/potential_request.%s.html' %
                                                  request.LANGUAGE_CODE, {'order_id': self.id})
            service.user.email_user(subject=_('Potential request'), html_message=potential_template)

    def __str__(self):
        return self.title


class Offer(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='offers', on_delete=models.CASCADE)
    order = models.ForeignKey(Order, related_name='offers', on_delete=models.CASCADE)

    comment = models.TextField(_('Covering letter'), max_length=1000)
    amount = models.PositiveIntegerField(_('Budget proposal'))

    created_at = models.DateTimeField(default=timezone.now)

    file = models.FileField(_('File'), blank=True, null=True, upload_to='offers/')

    is_declined = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Order offer'
        verbose_name_plural = 'Order offers'
        ordering = ['-created_at']

    def get_message_history(self):
        return reversed(Message.objects.filter(order_offer=self).order_by('-created_at')[:3])

    def get_new_messages(self):
        return Message.objects.filter(order=self.order, recipient=self.user, is_read=False)

    def __str__(self):
        return '{}: {}'.format(self.user.get_full_name(), textwrap.shorten(self.comment, width=30, placeholder=' ...'))


class Country(models.Model):
    name = models.CharField(_('Name'), max_length=150)
    position = models.PositiveIntegerField(_('Position'), default=0)

    class Meta:
        verbose_name = 'Country'
        verbose_name_plural = 'Countries'
        ordering = ['position', 'name']

    def __str__(self):
        return self.name


class City(models.Model):
    name = models.CharField(_('Name'), max_length=150)
    country = models.ForeignKey(Country, verbose_name=_('Country'), related_name='cities', on_delete=models.CASCADE)
    position = models.PositiveIntegerField(_('Position'), default=0)

    class Meta:
        verbose_name = 'City'
        verbose_name_plural = 'Cities'
        ordering = ['position', 'name']

    def __str__(self):
        return self.name


class Review(models.Model):
    order = models.OneToOneField(Order, verbose_name=_('Order'), on_delete=models.CASCADE)
    description = models.CharField(_('Description'), max_length=500, blank=True)
    rating = models.SmallIntegerField(_('Rating'), null=True, blank=True, validators=[
        validators.MinValueValidator(0), validators.MaxValueValidator(5)
    ])

    class Meta:
        verbose_name = 'Review'
        verbose_name_plural = 'Reviews'


class Message(models.Model):
    order = models.ForeignKey(Order, verbose_name=_('Order'), related_name='messages', blank=True, null=True,
                              on_delete=models.CASCADE)
    service = models.ForeignKey(UserService, verbose_name=_('Service'), related_name='messages', blank=True, null=True,
                                on_delete=models.CASCADE)

    sender = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('From user'), related_name='sent_messages',
                               on_delete=models.CASCADE)
    recipient = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('To user'), related_name='received_messages',
                                  on_delete=models.CASCADE)

    text = models.CharField(_('Message'), max_length=500)

    created_at = models.DateTimeField(auto_now_add=True)

    is_read = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Message'
        verbose_name_plural = 'Messages'
        ordering = ['-created_at']

    def __str__(self):
        return '{} -> {}: {}'.format(self.sender, self.recipient,
                                     textwrap.shorten(self.text, width=30, placeholder=' ...'))


class Invite(models.Model):
    token = models.CharField(_('Token'), max_length=500, editable=False, blank=True, unique=True)
    scheduled_send_at = models.DateTimeField(_('Планируемое время отправки'))
    send_at = models.DateTimeField(_('Дата фактической отправки'), editable=False, null=True, blank=True)
    registered_at = models.DateTimeField(_('Дата фактической регистрации'), editable=False, null=True, blank=True)

    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)

    email = models.EmailField(_('email address'))
    phone = models.BigIntegerField(verbose_name=_('Телефон пользователя'), help_text=_('Only digits'),
                                   null=True, blank=True)

    is_customer = models.BooleanField(default=False, verbose_name=_('Customer'))
    is_provider = models.BooleanField(default=False, verbose_name=_('Provider'))

    country = models.ForeignKey('Country', verbose_name=_('Country'), null=True, blank=True, on_delete=models.CASCADE)
    city = models.ForeignKey('City', verbose_name=_('City'), null=True, blank=True, on_delete=models.CASCADE)

    company_name = models.CharField(_('Company name'), max_length=200, blank=True)
    company_phone = models.BigIntegerField(_('Company phone'), help_text=_('В формате 79999999999'), null=True,
                                           blank=True)
    physical_address = models.CharField(_('Физический адрес офиса'), max_length=250, blank=True)
    website = models.URLField(_('Web site'), blank=True)
    logo = models.ImageField(_('logo'), upload_to=get_uploader('invites', 'logos'), null=True, blank=True)

    class Meta:
        verbose_name = 'Invite'
        verbose_name_plural = 'Invites'

    @classmethod
    def get_actual(cls, qs=None, **kwargs):
        kwargs.update(send_at__isnull=True, registered_at__isnull=True)
        if qs is None:
            qs = cls.objects.all()

        return qs.filter(**kwargs)

    def send(self, subject, message, from_email=None, **kwargs):
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def save(self, *args, **kwargs):
        if not self.token:
            self.token = crypto.invite_signer.sign(crypto.gen_password())
        return super().save(*args, **kwargs)

    def __str__(self):
        return '{}: {} {}'.format(
            self.email,
            self.scheduled_send_at.strftime('%d.%m.%Y %H:%M'),
            '✓' if self.send_at else '\u231B'
        )


class Office(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('User'), related_name='offices',
                             on_delete=models.CASCADE, blank=True, null=True)
    pre_user = models.ForeignKey(PreUser, related_name='offices', on_delete=models.CASCADE, blank=True, null=True)

    email = models.EmailField(_('email address'))
    phone = models.CharField(max_length=60)

    legal_address = models.CharField(_('Юридический адрес'), max_length=250)
    actual_address = models.CharField(_('Фактический адрес'), max_length=250)

    country = models.ForeignKey(Country, related_name='offices', verbose_name=_('Country'),
                                on_delete=models.CASCADE)
    city = models.ForeignKey(City, related_name='offices', verbose_name=_('City'), on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Office'
        verbose_name_plural = 'Offices'

    def __str__(self):
        return '{}: {}'.format(self.user, textwrap.shorten(self.legal_address, width=30, placeholder=' ...'))


class DataCenter(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('User'), related_name='data_centers',
                             on_delete=models.CASCADE, blank=True, null=True)
    pre_user = models.ForeignKey(PreUser, on_delete=models.CASCADE, blank=True, null=True, related_name='data_centers')

    name = models.CharField(_('Name'), max_length=250)

    country = models.ForeignKey(Country, related_name='data_centers', verbose_name=_('Country'),
                                on_delete=models.CASCADE)
    city = models.ForeignKey(City, related_name='data_centers', verbose_name=_('City'), on_delete=models.CASCADE)

    address = models.CharField(_('Address'), max_length=250)

    class Meta:
        verbose_name = 'DataCenter'
        verbose_name_plural = 'DataCenters'

    def __str__(self):
        return '{}: {}'.format(self.user, textwrap.shorten(self.address, width=30, placeholder=' ...'))


class UserServiceInvite(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='invites', on_delete=models.CASCADE)
    order = models.ForeignKey(Order, related_name='invites', on_delete=models.CASCADE)

    service = models.ForeignKey(UserService, related_name='invites', on_delete=models.CASCADE)


class New(models.Model):
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    title = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255)

    short_text = RichTextUploadingField()

    text = RichTextUploadingField()

    published_at = models.DateTimeField()

    created_at = models.DateTimeField(auto_now_add=True)

    language = models.CharField(choices=const.LANGUAGE_CHOICES, default=const.EN, max_length=10)

    class Meta:
        verbose_name = 'News'
        verbose_name_plural = 'News'
        ordering = ['-published_at']

    def __str__(self):
        return self.title


class SubCategoryField(models.Model):
    CHAR_FIELD = 'char_field'
    TEXT_FIELD = 'text_field'
    INTEGER_FIELD = 'integer_field'
    CHOICE_FIELD = 'select_field'
    MULTIPLE_CHOICE_FIELD = 'multiple_select_field'
    RANGE_FIELD = 'range_field'

    TYPE_CHOICES = (
        (CHAR_FIELD, 'Char field'),
        (TEXT_FIELD, 'Text field'),
        (INTEGER_FIELD, 'Integer field'),
        (CHOICE_FIELD, 'Select field'),
        (MULTIPLE_CHOICE_FIELD, 'Multiple select field'),
        (RANGE_FIELD, 'Range field'),
    )

    label = models.CharField(max_length=255)
    sub_category = models.ForeignKey(SubCategory, on_delete=models.CASCADE)
    field_type = models.CharField(choices=TYPE_CHOICES, max_length=40)
    language = models.CharField(choices=const.LANGUAGE_CHOICES, max_length=10)
    required = models.BooleanField(default=0)
    help_text = models.CharField(max_length=255, blank=True)

    select_choices = models.TextField(blank=True,
                                      help_text='Required for select field, enter values separated by commas')

    max = models.IntegerField(blank=True, null=True, help_text='Required for range field')
    min = models.IntegerField(blank=True, null=True, help_text='Required for range field')
    step = models.IntegerField(blank=True, null=True, help_text='Required for range field')

    position = models.IntegerField(verbose_name='Position',
                                   blank=True, null=True,
                                   help_text='Field position in template. '
                                             'Disable auto position if you want custom position')
    auto_pos = models.BooleanField(default=1, verbose_name='Auto position')

    class Meta:
        verbose_name = 'SubCategory Field'
        verbose_name_plural = 'SubCategory Fields'
        unique_together = ('label', 'sub_category', 'language')
        ordering = ('position',)

    def __str__(self):
        return '%s %s %s' % (self.sub_category.name, self.label, self.field_type)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if self.auto_pos:
            try:
                prev_pos = SubCategoryField.objects.filter(sub_category=self.sub_category,
                                                           language=self.language).exclude(id=self.id).latest(
                    'id').position
                self.position = prev_pos + 1
            except Exception as e:
                self.position = 0
        super().save(*args, **kwargs)


class SubCategoryFieldValue(models.Model):
    field = models.ForeignKey(SubCategoryField, on_delete=models.CASCADE)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    value = models.CharField(max_length=255)

    class Meta:
        verbose_name = 'SubCategory Field Value'
        verbose_name_plural = 'SubCategory Field Values'


class Notification(models.Model):
    recipient = models.ForeignKey(User, on_delete=models.CASCADE)

    message = models.TextField(max_length=255)
    is_read = models.BooleanField(default=0)
    created_at = models.DateTimeField(default=timezone.now)

    class Meta:
        verbose_name = 'Notification'
        verbose_name_plural = 'Notifications'


class Feedback(models.Model):
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='feedbacks_created')
    recipient = models.ForeignKey(User, on_delete=models.CASCADE, related_name='feedbacks_received')
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='feedbacks')

    quality = models.FloatField(default=0)
    time = models.FloatField(default=0)
    communication = models.FloatField(default=0)
    total_stars = models.FloatField(blank=True, null=True)

    is_recommended = models.BooleanField(default=True)
    text = models.TextField()

    created_at = models.DateTimeField(default=timezone.now)

    class Meta:
        verbose_name = 'Feedback'
        verbose_name_plural = 'Feedbacks'
        ordering = ('-created_at',)

    def update_data(self):
        self.total_stars = self.quality + self.communication + self.time
        self.recipient.set_rating()
        return self.recipient.rating


class Faq(models.Model):
    language = models.CharField(choices=const.LANGUAGE_CHOICES, max_length=10)

    question = models.CharField(max_length=500)
    answer = RichTextUploadingField()

    position = models.PositiveSmallIntegerField(default=0)

    class Meta:
        verbose_name = 'FAQ'
        verbose_name_plural = 'FAQ'
        ordering = ('-position',)


class Terms(models.Model):
    language = models.CharField(choices=const.LANGUAGE_CHOICES, max_length=10)
    text = RichTextField()

    class Meta:
        verbose_name = 'Terms'
        verbose_name_plural = 'Terms'


class TermsFile(models.Model):
    term = models.ForeignKey(Terms, related_name='files', on_delete=models.CASCADE)
    description = models.CharField(max_length=250)
    file = models.FileField(upload_to='terms/')

    class Meta:
        verbose_name = 'TermsFile'
        verbose_name_plural = 'TermsFile'


class Privacy(models.Model):
    language = models.CharField(choices=const.LANGUAGE_CHOICES, max_length=10)
    text = RichTextField()

    class Meta:
        verbose_name = 'Privacy'
        verbose_name_plural = 'Privacy'


class PrivacyFile(models.Model):
    privacy = models.ForeignKey(Privacy, related_name='files', on_delete=models.CASCADE)
    description = models.CharField(max_length=250)
    file = models.FileField(upload_to='privacy/')

    class Meta:
        verbose_name = 'PrivacyFile'
        verbose_name_plural = 'PrivacyFile'


def slider_upload_to(instance, filename):
    *filenames, ext = filename.split('.')
    return '/'.join(['slider', slugify('_'.join(filenames)) + '.' + ext])


class Slider(models.Model):
    language = models.CharField(choices=const.LANGUAGE_CHOICES, max_length=10)
    image = models.ImageField(upload_to=slider_upload_to, verbose_name='Изображение')
    header = models.CharField(max_length=100, verbose_name='Заголовок')
    description = models.CharField(max_length=200, verbose_name='Описание')
    action_text = models.CharField(blank=True, max_length=100, verbose_name='Текст кнопки')
    action_to = models.CharField(blank=True, max_length=100, verbose_name='Ссылка')
    query_string = models.CharField(blank=True, max_length=250)

    show_on_website = models.BooleanField(default=False, verbose_name='Показывать на сайте')

    position = models.PositiveSmallIntegerField(default=0, verbose_name='Приоритет сортировки')

    class Meta:
        ordering = ['position']
        verbose_name = 'слайдер'
        verbose_name_plural = 'Слайдер'

    def __str__(self):
        return self.header


class SubcategoryPreview(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    sub_category = models.ForeignKey(SubCategory, on_delete=models.CASCADE)

    position = models.SmallIntegerField(default=0)
    lang = models.CharField(max_length=255, choices=LANGUAGES)

    name = models.CharField(max_length=255)
    description = models.TextField()
    preview = models.FileField(upload_to='index/order/')

    class Meta:
        ordering = ['position']
        verbose_name = 'Subcategory preview'
        verbose_name_plural = 'Subcategory preview'


class Tariff(models.Model):
    FREE = 'free'
    PRO = 'pro'

    TYPE_CHOICES = (
        (FREE, 'Free'),
        (PRO, 'Pro')
    )

    name = models.CharField(max_length=255)
    description = RichTextField(max_length=400)

    type = models.CharField(choices=TYPE_CHOICES, default=FREE, max_length=255)

    language = models.CharField(choices=const.LANGUAGE_CHOICES, default=const.EN, max_length=10)

    price = models.FloatField(default=0)
    currency = models.CharField(choices=const.CURRENCY_CHOICES, max_length=255)
    replies = models.SmallIntegerField(default=0)

    position = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name = 'Tariff'
        verbose_name_plural = 'Tariffs'
