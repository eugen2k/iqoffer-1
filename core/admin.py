import string

from django.contrib import admin
from django.contrib.auth.models import Group
from django.urls import reverse_lazy

from core import models
from project.settings import STATIC_URL


class AlphabetFilter(admin.SimpleListFilter):
    title = 'alphabet'
    parameter_name = 'alphabet'

    def lookups(self, request, model_admin):
        en = list(string.ascii_lowercase)
        ru = [chr(symb) for symb in range(1040, 1072)]
        return ((c.upper(), c.upper()) for c in en + ru)

    def queryset(self, request, queryset=None):
        if self.value():
            return queryset.filter(name__startswith=self.value())


class SubCategoryInline(admin.StackedInline):
    model = models.SubCategory
    extra = 1


class CityInline(admin.StackedInline):
    model = models.City
    extra = 1


class UserServiceInline(admin.StackedInline):
    model = models.UserService
    extra = 1


class CountryModelAdmin(admin.ModelAdmin):
    inlines = (CityInline,)
    list_display = ('name', 'position')


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'position',)
    inlines = (SubCategoryInline,)
    list_filter = (AlphabetFilter,)


class SubCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'category', 'description', 'position',)
    list_filter = ('category',)


class BusinessCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'position')


class UserAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'email', 'username', 'is_active', 'is_staff', 'is_superuser', 'is_customer', 'is_provider',
        'last_online_at', 'selected_lang', 'timezone')


class UserServiceAdmin(admin.ModelAdmin):
    list_display = ('user', 'name', 'description')

    class Media:
        js = (STATIC_URL + 'core/js/admin_cities_manager.js',)


class CityAdmin(admin.ModelAdmin):
    list_display = ('name', 'country', 'position')
    list_filter = ['country__name']


class NewAdmin(admin.ModelAdmin):
    list_display = ('title', 'created_by', 'published_at', 'created_at', 'language')
    list_filter = ('language',)
    prepopulated_fields = {"slug": ("title",)}


class SubCategoryFieldAdmin(admin.ModelAdmin):
    list_display = ('label', 'sub_category', 'field_type', 'language', 'required', 'position', 'help_text',)
    list_filter = ('sub_category', 'field_type', 'language', 'required')


class ValueAdmin(admin.ModelAdmin):
    list_display = ('order_id', 'field', 'value')


class ServiceInline(admin.TabularInline):
    model = models.UserService


class DataCenterInline(admin.TabularInline):
    model = models.DataCenter


class ContactsInline(admin.TabularInline):
    model = models.UserContact
    max_num = 1
    extra = 1


class OfficeInline(admin.TabularInline):
    model = models.Office


class RequisitesInline(admin.TabularInline):
    model = models.Requisites
    max_num = 1
    extra = 1


class PreUserAdmin(admin.ModelAdmin):
    list_display = (
        'email', 'captcha_score', 'username', 'from_username', 'from_email', 'show_in_stat', 'is_customer',
        'is_provider', 'from_site', 'user', 'selected_lang', 'scheduled_send_at', 'send_at', 'send_res',
        'created_at',)
    inlines = [
        ServiceInline, DataCenterInline, OfficeInline, RequisitesInline, ContactsInline,
    ]
    list_filter = ('from_site', 'captcha_score', 'show_in_stat',)

    class Media:
        js = (STATIC_URL + 'core/js/subcategory_admin.js', STATIC_URL + 'core/js/admin_cities_manager_inline.js',)


class FaqAdmin(admin.ModelAdmin):
    list_display = ('question', 'language', 'position')
    list_filter = ('question',)


class TermsFileInline(admin.TabularInline):
    model = models.TermsFile


class TermsAdmin(admin.ModelAdmin):
    inlines = [TermsFileInline]
    list_display = ('id', 'language')


class PrivacyFileInline(admin.TabularInline):
    model = models.PrivacyFile


class PrivacyAdmin(admin.ModelAdmin):
    inlines = [PrivacyFileInline]
    list_display = ('id', 'language')


class LegalProvisionAdmin(admin.ModelAdmin):
    list_display = ('name', 'language')
    list_filter = ('language',)


class DataCenterAdmin(admin.ModelAdmin):
    class Media:
        js = (STATIC_URL + 'core/js/admin_cities_manager.js',)


class OfficeAdmin(admin.ModelAdmin):
    class Media:
        js = (STATIC_URL + 'core/js/admin_cities_manager.js',)


class SliderAdmin(admin.ModelAdmin):
    list_display = ('language', 'header', 'show_on_website', 'position', 'action_to', 'action_text')


class SubcategoryPreviewAdminn(admin.ModelAdmin):
    list_display = ('lang', 'name', 'position', 'category', 'sub_category')


class AdminMessage(admin.ModelAdmin):
    list_display = ('sender', 'recipient', 'order', 'service', 'text', 'is_read', 'created_at')


class AdminTariff(admin.ModelAdmin):
    list_display = ('name', 'position', 'language', 'type', 'price', 'currency', 'replies', 'description', )


admin.site.register(models.Terms, TermsAdmin)
admin.site.register(models.Privacy, PrivacyAdmin)

admin.site.register(models.User, UserAdmin)
admin.site.register(models.PreUser, PreUserAdmin)
admin.site.register(models.UserService, UserServiceAdmin)
admin.site.register(models.UserServiceInvite)

admin.site.register(models.Order)
admin.site.register(models.Offer)

admin.site.register(models.BusinessCategory, BusinessCategoryAdmin)
admin.site.register(models.Category, CategoryAdmin)
admin.site.register(models.SubCategory, SubCategoryAdmin)

admin.site.register(models.Country, CountryModelAdmin)
admin.site.register(models.City, CityAdmin)

admin.site.register(models.LegalProvision, LegalProvisionAdmin)

admin.site.register(models.Review)
admin.site.register(models.Message, AdminMessage)
admin.site.register(models.Notification)

admin.site.register(models.Office, OfficeAdmin)
admin.site.register(models.DataCenter, DataCenterAdmin)

admin.site.register(models.New, NewAdmin)
admin.site.register(models.Requisites)
admin.site.register(models.UserContact)

admin.site.register(models.SubCategoryField, SubCategoryFieldAdmin)
admin.site.register(models.SubCategoryFieldValue, ValueAdmin)

admin.site.register(models.Feedback)
admin.site.register(models.Tariff, AdminTariff)

admin.site.register(models.Faq, FaqAdmin)
admin.site.register(models.Slider, SliderAdmin)

admin.site.unregister(Group)
admin.site.register(models.SubcategoryPreview, SubcategoryPreviewAdminn)
