from django.core import signing

try:
    import secrets

    def gen_password(length: int = 8) -> str:
        return secrets.token_urlsafe(length)

except ImportError:
    import random
    import string

    def gen_password(length: int = 8) -> str:
        alphabet = list(string.ascii_letters + string.digits)
        password = ''

        for _ in range(length):
            random.shuffle(alphabet)
            password += random.choice(alphabet)

        return password

registration_signer = signing.Signer(salt='-jGl-b7xBN2IbTNvb2TrdE1zfpVojmRvY-R5OYqyB2U')
reset_password_signer = signing.Signer(salt='BKSlTPmPB-p6dkjo_1rsyc-ws-G7ZxFPW0AA-IUM3bw')
invite_signer = signing.Signer(salt='_sfa16p0ZUhB2ZSKRWz9iYa40Q_gMRYkUdYhO0XXWwI')
