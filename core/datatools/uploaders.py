import os
from functools import partial

from django.utils.text import slugify


def get_uploader(*path):
    return partial(uploader, path=path)


def uploader(instance, filename, path):
    filename, extension = os.path.splitext(filename)

    return os.path.join(*path, '{}{}'.format(slugify(filename), extension))
