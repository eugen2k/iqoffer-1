RU = 'ru'
EN = 'en'

LANGUAGE_CHOICES = (
    (EN, 'English'),
    (RU, 'Russian'),
)

EUR = 'eur'
USD = 'usd'
RUB = 'rub'

CURRENCY_CHOICES = (
    (EUR, 'EUR'),
    (USD, 'USD'),
    (RUB, 'RUB')
)
