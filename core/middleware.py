import datetime

import pytz
from django.contrib import messages
from django.utils import timezone
from django.http import Http404, HttpResponse
from django.shortcuts import redirect, render
from django.urls import reverse_lazy, resolve
from django.contrib.messages import get_messages
from django.utils.translation import ugettext_lazy as _

from core import models
from project.settings import DEBUG


def add_message(request, level, message):
    if message not in [m.message for m in get_messages(request)]:
        getattr(messages, level)(request, message)


class CheckPerformer:
    exempt_urls = ('logout', 'set_language', 'new', 'get_subcategories')
    exempt_path = ('/new', '/media', '/static', '/language', '/ajax', '/ru/ajax', '/ru/language')

    add_requisites = reverse_lazy('requisites')
    add_service = reverse_lazy('service_create')
    add_office = reverse_lazy('office_create')
    add_contact = reverse_lazy('contact')
    add_dc = reverse_lazy('dc_create')

    def __init__(self, get_response):
        self.get_response = get_response

    def check_user(self, user, request):
        if resolve(request.path).app_name != 'admin' and resolve(request.path).url_name not in self.exempt_urls \
                and not any(request.path.startswith(path) for path in self.exempt_path):
            if not user.offices.exists():
                if user.is_provider:
                    add_message(request, 'warning', _('Your contact details will be visible to customers'))
                if user.is_customer:
                    add_message(request, 'warning',
                                _('Your contact details will be visible to the providers according to your requests'))
                return redirect(self.add_office)
            elif not user.services.exists():
                add_message(request, 'warning', _('In order to continue, you need to add at least one service'))
                return redirect(self.add_service)
            elif not hasattr(user, 'requisites') or not all(
                    getattr(user.requisites, field.name) for field in user.requisites.__class__._meta.get_fields() if
                    field.name == 'company_name' or field.name == 'legal_provision'):
                add_message(request, 'warning', _('In order to continue, you need to add the company info'))
                return redirect(self.add_requisites)
            elif not hasattr(user, 'contacts') or (not user.contacts.email and not user.contacts.phone):
                add_message(request, 'warning',
                            _('In order to continue, you need to add the contacts'))
                return redirect(self.add_contact)

    def prepare_response(self, request, response):
        if not response or request.path == response.url:
            response = self.get_response(request)
        return response

    def __call__(self, request):
        user = models.User.objects.filter(pk=request.user.pk).select_related().first()
        response = None

        if request.user.is_authenticated and user.is_provider:
            response = self.check_user(user=user, request=request)

        return self.prepare_response(request, response)


class CRUDPermission:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        forbidden_urls = []
        user = request.user

        if resolve(request.path).url_name in forbidden_urls and not user.is_staff and not user.is_superuser:
            return redirect(reverse_lazy('new_list'))

        response = self.get_response(request)

        return response


class Perms:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        performer_forbidden_urls = ('my_order_list', 'order_create', 'order_update', 'order_delete', 'offer_accept')
        customer_forbidden_urls = ('offer_list',)
        customer_forbidden_paths = ('/settings/dc', '/settings/service')
        user = request.user

        if not user.is_authenticated:
            pass

        elif user.is_provider and user.is_customer:
            pass

        elif user.is_provider and resolve(request.path).url_name in performer_forbidden_urls:
            messages.info(request, _('The requested page is only available to customers'))
            return redirect(reverse_lazy('profile', kwargs={'pk': user.id}))

        elif user.is_customer and (resolve(request.path).url_name in customer_forbidden_urls or request.path.startswith(
                customer_forbidden_paths)):
            messages.info(request, _('The requested page is only available to providers'))
            return redirect(reverse_lazy('profile', kwargs={'pk': user.id}))

        response = self.get_response(request)

        return response


class SetLastOnlineMiddleware:
    CHECK_INTERVAL = datetime.timedelta(minutes=3)

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.user.is_authenticated:
            last_online = request.user.last_online_at

            if not last_online or last_online < timezone.now() - self.CHECK_INTERVAL:
                request.user.last_online_at = timezone.now()
                request.user.save()

        return self.get_response(request)


class TimeZoneMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        user = request.user

        if user.is_authenticated:
            tz = user.timezone
            timezone.activate(pytz.timezone(tz))

        response = self.get_response(request)

        return response
