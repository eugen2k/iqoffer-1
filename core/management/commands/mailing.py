import time
from urllib import parse

import datetime
from django.core.management.base import BaseCommand
from django.template.loader import render_to_string
from django.utils.timezone import now
from django.urls import reverse, reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.utils import translation
from django.conf import settings
from core import models


class Command(BaseCommand):
    help = 'Sends invites.'

    def add_arguments(self, parser):
        parser.add_argument('-i', '--infinitely', dest='infinitely', action='store_true')

    def handle(self, *args, **options):
        infinitely = options.get('infinitely')
        registration_url = parse.urljoin(settings.SERVER_HOSTNAME, reverse('registration'))

        while True:
            for pre_user in models.PreUser.get_actual():
                registration_url_parts = list(parse.urlparse(registration_url))
                registration_url_parts[4] = parse.urlencode(dict(invite=str(pre_user.token)))
                invite_url = parse.urlunparse(registration_url_parts)

                invite_template = render_to_string('core/emails/pre_user_invite.%s.html' %
                                                   pre_user.selected_lang, {'invite_url': invite_url, 'user': pre_user})
                with translation.override(pre_user.selected_lang):
                    send_res = pre_user.email_user(subject=_('Invite to iqOffer'), html_message=invite_template)
                    pre_user.send_res = str(send_res)
                pre_user.send_at = now()
                pre_user.save()

            for order in models.Order.objects.filter(deadline_mailed_at__isnull=True,
                                                     offer_deadline__isnull=False, offer__isnull=True):
                if now() > order.offer_deadline:
                    owner = order.owner
                    deadline_template = render_to_string('core/emails/order_deadline.%s.html' %
                                                         owner.selected_lang, {'order_id': order.pk})

                    with translation.override(owner.selected_lang):
                        subject = _('Your request is expired')

                    order.owner.email_user(subject=subject, html_message=deadline_template)
                    order.deadline_mailed_at = now()
                    order.save()

            if infinitely:
                time.sleep(settings.INVITE_SEND_WAITING.total_seconds())
            else:
                return
