from django.core.management.base import BaseCommand

from core.models import Country, SubCategory, Category, City, BusinessCategory


class Command(BaseCommand):

    def handle(self, *args, **options):
        with open('trans/templates/trans/default.inc.html', 'w+') as tmpl:
            self.tmpl = tmpl
            self.tmpl.truncate(0)

            for country in Country.objects.all():
                self.write(country.name)

            for city in City.objects.all():
                self.write(city.name)

            for category in Category.objects.all():
                self.write(category.name)

            for subcategory in SubCategory.objects.all():
                self.write(subcategory.name)

            for cat in BusinessCategory.objects.all():
                self.write(cat.name)

    def write(self, content):
        to_insert = "{%% trans '%s' %%} <br/>\n" % content
        self.tmpl.write(to_insert)
