from django.conf.urls import url

from core import views
from core.views import user, message, settings, new, ajax
from django.conf import settings as django_settings

from core.views import offer, other, order

from core.views.settings import dc
from core.views.settings import office
from core.views.settings import service
from core.views.other import dashboard

from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', views.Index.as_view(), name='index'),

    url(r'^dashboard/$', dashboard, name='dashboard'),

    url(r'^orders/$', order.OrderList.as_view(), name='order_list'),
    url(r'^orders/my/$', order.UserOrderList.as_view(), name='my_order_list'),
    url(r'^orders/create/$', order.OrderCreate.as_view(), name='order_create'),
    url(r'^orders/(?P<pk>\d+)/delete/$', order.OrderDeleteView.as_view(), name='order_delete'),
    url(r'^orders/(?P<pk>\d+)/$', order.OrderDetail.as_view(), name='order_detail'),
    url(r'^orders/(?P<pk>\d+)/update/$', order.OrderUpdate.as_view(), name='order_update'),
    url(r'^orders/(?P<pk>\d+)/offer/(?P<offer_pk>\d+)/accept/$', offer.OfferAcceptUpdateView.as_view(),
        name='offer_accept'),

    url(r'^orders/(?P<pk>\d+)/feedback/$', order.OrderFeedbackCreateView.as_view(), name='order_feedback'),

    url(r'^offers/$', offer.OfferList.as_view(), name='offer_list'),
    url(r'^order/(?P<order_pk>\d+)/offer/(?P<offer_pk>\d+)/$', offer.OfferDelete.as_view(), name='offer_delete'),
    url(r'^offer/(?P<pk>\d+)/decline/$', offer.OfferDecline.as_view(), name='offer_decline'),

    url(r'^services/$', user.UserServiceList.as_view(), name='full_service_list'),
    url(r'^services/responses/$', user.CustomerServiceResponseList.as_view(), name='services_responses'),
    url(r'^service/(?P<pk>\d+)/$', user.UserServiceDetail.as_view(), name='service'),
    url(r'^service/(?P<pk>\d+)/invite/$', user.InviteUserToOrderCreateView.as_view(),
        name='service_invite'),

    url(r'^service/(?P<service_pk>\d+)/messages/recipient/(?P<recipient_pk>\d+)/$',
        message.ServiceMessagesDetail.as_view(),
        name='service_messages'),
    url(r'^orders/(?P<order_pk>\d+)/messages/recipient/(?P<recipient_pk>\d+)/$', message.OrderMessagesDetail.as_view(),
        name='order_messages'),
    url(r'^message/save/$', message.save_message,
        name='message_save'),

    url(r'^settings/requisites/$', settings.Requisites.as_view(), name='requisites'),
    url(r'^settings/contact/$', settings.Contact.as_view(), name='contact'),
    url(r'^settings/profile/$', settings.Profile.as_view(), name='profile_settings'),
    url(r'^profile/(?P<pk>\d+)/$', user.Profile.as_view(), name='profile'),

    url(r'^settings/offices/create/$', office.OfficeCreate.as_view(), name='office_create'),
    url(r'^settings/offices/$', office.OfficeList.as_view(), name='office_list'),
    url(r'^settings/offices/(?P<pk>\d+)/update/$', office.OfficeUpdate.as_view(), name='office_update'),
    url(r'^settings/offices/(?P<pk>\d+)/delete/$', office.OfficeDelete.as_view(), name='office_delete'),

    url(r'^settings/dc/create/$', dc.DCCreate.as_view(), name='dc_create'),
    url(r'^settings/dc/$', dc.DCList.as_view(), name='dc_list'),
    url(r'^settings/dc/(?P<pk>\d+)/update/$', dc.DCUpdate.as_view(), name='dc_update'),
    url(r'^settings/dc/(?P<pk>\d+)/delete/$', dc.DCDelete.as_view(), name='dc_delete'),

    url(r'^settings/service/create/$', service.ServiceCreate.as_view(), name='service_create'),
    url(r'^settings/service/$', service.ServiceList.as_view(), name='service_list'),
    url(r'^settings/service/(?P<pk>\d+)/update/$', service.ServiceUpdate.as_view(),
        name='service_update'),
    url(r'^settings/service/(?P<pk>\d+)/delete/$', service.ServiceDelete.as_view(),
        name='service_delete'),

    url(r'^news/$', new.NewListView.as_view(), name='new_list'),
    url(r'^news/(?P<slug>[0-9a-zA-Z_-]{1,255})/$', new.NewDetailView.as_view(), name='new_detail'),

    url(r'^ajax/get/subcategories/$', ajax.get_sub_categories, name='get_subcategories'),
    url(r'^ajax/get/subcategory/initial/fields/$', ajax.get_initial_subcategory_fields,
        name='get_subcategory_initial_fields'),
    url(r'^ajax/get/subcategory/bound/fields/$', ajax.get_bound_subcategory_fields,
        name='get_subcategory_bound_fields'),
    url(r'^ajax/get/categories/selector/$', ajax.get_categories_selector, name='get_categories_selector'),

    url(r'^ajax/get/notifications/count/$', ajax.get_notifications_count,
        name='get_notifications_count'),
    url(r'^ajax/set/notifications/read/$', ajax.set_read_to_notifications,
        name='set_notifications_read'),

    url(r'^ajax/get/messages/count/$', ajax.get_messages_count,
        name='get_messages_count'),

    url(r'^ajax/get/cities/$', ajax.get_cities,
        name='get_cities'),

    url(r'^language/change/$', other.change_language, name='change_language'),

    url(r'^tariff/$', other.TariffListView.as_view(), name='tariff'),
    url(r'^faq/$', other.faq, name='faq'),
    url(r'^terms/$', other.terms, name='terms'),
    url(r'^privacy/$', other.privacy, name='privacy'),

]

urlpatterns += static(django_settings.MEDIA_URL, document_root=django_settings.MEDIA_ROOT)
