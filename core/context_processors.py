from django.conf import settings
from core.models import Message, User, Notification


def order_new_messages_count(request):
    messages = 0
    if request.user.is_authenticated:
        messages = Message.objects.filter(recipient=request.user, is_read=False, order__isnull=False).count()
    return {'order_new_messages_count': messages}


def service_new_messages_count(request):
    messages = 0
    if request.user.is_authenticated:
        messages = Message.objects.filter(recipient=request.user, is_read=False, service__isnull=False).count()
    return {'service_new_messages_count': messages}


def notifications(request):
    notifs = []
    if request.user.is_authenticated:
        notifs = Notification.objects.filter(recipient=request.user).order_by('-created_at')
    return {'notifications': notifs}
