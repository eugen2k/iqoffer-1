from django.contrib import messages
from django.shortcuts import redirect, render
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.utils import translation

from core.views import core_generic
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

from core import models
from core import forms
from core.views.other import was_read
from project.settings import SERVER_HOSTNAME


def save_message(request):
    post = request.POST.copy()
    request_from = post.pop('path')[0]
    form = forms.Message(post)
    if form.is_valid():
        message = form.save(commit=False)
        order = message.order
        service = message.service
        sender = message.sender
        recipient = message.recipient

        with translation.override(recipient.selected_lang):
            unknown_customer_message = False

            if order:
                if order.show_contacts_only_for_winner and recipient == order.owner and sender != order.winner:
                    unknown_customer_message = _(
                        'Please note, your contact info will be visible only to request winner')
                    subject = _('Request {order_id} message from {sender}').format(
                        **{'order_id': order.id, 'sender': sender})
                elif order.show_contacts_only_for_winner and recipient != order.winner and sender != order.winner:
                    unknown_customer_message = _(
                        'Please note, customer contact info will be available only to request winner')
                    subject = _('Request {order_id} message').format(**{'order_id': order.id})
                else:
                    subject = _(
                        'Request {order_id} message from {sender}').format(**{'order_id': order.id, 'sender': sender})

            if service:
                subject = _('Service message from {sender}').format(**{'sender': sender})

            template = render_to_string('core/emails/message.%s.html' %
                                        recipient.selected_lang,
                                        {'message': message, 'unknown_customer_message': unknown_customer_message})

            recipient.email_user(subject=subject, html_message=template)

        message.save()
        messages.success(request, _('Message was successfully sent'))
    return redirect(request_from)


class MessagesDetail:

    def get_message_form(self):
        return forms.Message(self.request.POST or None)


class OrderMessagesDetail(MessagesDetail, core_generic.View):
    recipient = None
    order = None

    def get(self, request, *args, **kwargs):
        return render(request, 'core/messages/order_detail.html', self.get_context_data(**kwargs))

    def get_messages_history(self):
        recipient = self.get_recipient()
        order = self.get_order()
        mess = models.Message.objects.filter(
            Q(recipient=recipient, sender=self.request.user) | Q(recipient=self.request.user, sender=recipient),
            order=order).order_by('created_at')
        was_read(mess, self.request.user)
        return mess

    def get_recipient(self):
        if not self.recipient:
            self.recipient = models.User.objects.get(id=self.kwargs.get('recipient_pk'))
        return self.recipient

    def get_order(self):
        if not self.order:
            self.order = models.Order.objects.get(id=self.kwargs.get('order_pk'))
        return self.order

    def is_show_head(self):
        order = self.get_order()
        if not order.show_contacts_only_for_winner or self.request.user == order.owner or self.request.user == order.winner:
            return True
        return False

    def get_context_data(self, **kwargs):
        context = dict()
        context['recipient'] = self.get_recipient()
        context['messages_history'] = self.get_messages_history()
        context['order'] = self.get_order()
        context['message_form'] = self.get_message_form()

        context['show_head'] = self.is_show_head()
        context['scrollable'] = True
        return context


class ServiceMessagesDetail(MessagesDetail, core_generic.View):
    recipient = None
    service = None

    def get(self, request, *args, **kwargs):
        return render(request, 'core/messages/service_detail.html', self.get_context_data(**kwargs))

    def get_messages_history(self):
        recipient = self.get_recipient()
        service = self.get_service()
        mess = models.Message.objects.filter(
            Q(recipient=recipient, sender=self.request.user) | Q(recipient=self.request.user, sender=recipient),
            service=service).order_by('created_at')
        was_read(mess, self.request.user)
        return mess

    def get_recipient(self):
        if not self.recipient:
            self.recipient = models.User.objects.get(id=self.kwargs.get('recipient_pk'))
        return self.recipient

    def get_service(self):
        if not self.service:
            self.service = models.UserService.objects.get(id=self.kwargs.get('service_pk'))
        return self.service

    def get_context_data(self, **kwargs):
        context = dict()
        context['recipient'] = self.get_recipient()
        context['messages_history'] = self.get_messages_history()
        context['service'] = self.get_service()
        context['message_form'] = self.get_message_form()

        context['show_head'] = True
        context['scrollable'] = True
        return context
