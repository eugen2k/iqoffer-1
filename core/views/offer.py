from django.db.models import Q
from django.shortcuts import redirect, render
from django.template.loader import render_to_string
from django.urls import reverse, reverse_lazy
from django.utils import translation
from django.views import generic
from core.views import core_generic
from django.utils.translation import ugettext_lazy as _

from core import models
from core.forms import Message


class OfferList(core_generic.ListView):
    model = models.Order
    template_name = 'core/offer/order_list.html'
    paginate_by = 30
    context_object_name = 'orders'

    def get_queryset(self):
        user = self.request.user
        return self.model.objects.filter(
            Q(offers__in=user.offers.all()) | Q(messages__recipient=user) | Q(messages__sender=user)).distinct()

    def get_title(self):
        return _('My responses')


class OfferAcceptUpdateView(core_generic.View):

    def get(self, request, *args, **kwargs):
        return render(request, 'core/cun/notification.html', context=self.get_context_data())

    def get_order(self):
        return models.Order.objects.get(id=self.kwargs.get('pk'))

    def get_offer(self):
        return models.Offer.objects.get(id=self.kwargs.get('offer_pk'))

    def post(self, request, *args, **kwargs):
        offer = self.get_offer()
        order = self.get_order()
        order.offer = offer
        order.winner = offer.user
        order.save()

        provider_accepted_template = render_to_string('core/emails/offer_accepted_provider.%s.html' %
                                                      order.winner.selected_lang, {'order_id': order.pk})

        with translation.override(order.winner.selected_lang):
            order.winner.email_user(subject=_('Your offer is the best'), html_message=provider_accepted_template)

        customer_accepted_template = render_to_string('core/emails/offer_accepted_customer.%s.html' %
                                                      order.owner.selected_lang, {'order': order})
        with translation.override(order.owner.selected_lang):
            order.owner.email_user(
                subject=_('You have successfully found the IT provider on the request %s') % order.id,
                html_message=customer_accepted_template)

        for offer in models.Offer.objects.filter(order=order).exclude(user=order.winner):
            declined_template = render_to_string('core/emails/offer_declined.%s.html' %
                                                 request.LANGUAGE_CODE, {'order_id': order.pk})
            offer.user.email_user(subject=_('Offer declined'), html_message=declined_template)

        return redirect(reverse('order_detail', kwargs={'pk': self.kwargs.get('pk')}))

    def get_title(self):
        return _('Are you sure you want to accept this offer?')

    def get_context_data(self, **kwargs):
        context = dict()
        context['title'] = self.get_title()
        context['order'] = self.get_order()
        context['offer'] = self.get_offer()
        return context


class OfferDecline(generic.View):

    def get_offer(self):
        return models.Offer.objects.get(id=self.kwargs.get('pk'))

    def get(self, request, *args, **kwargs):
        title = _('Are you sure you want to decline this offer?')
        offer = self.get_offer()
        return render(request, 'core/cun/notification.html', {'title': title, 'offer': offer, 'order': offer.order})

    def post(self, request, *args, **kwargs):
        offer = self.get_offer()
        offer.is_declined = True
        offer.save()
        declined_template = render_to_string('core/emails/offer_declined.%s.html' %
                                             request.LANGUAGE_CODE, {'order_id': offer.order.pk})
        offer.user.email_user(subject=_('Offer declined'), html_message=declined_template)
        return redirect(reverse_lazy('order_detail', kwargs={'pk': offer.order.id}))


class OfferDelete(core_generic.DeleteView):
    model = models.Offer
    template_name = 'core/cun/notification.html'
    pk_url_kwarg = 'offer_pk'

    def get_success_url(self):
        return reverse_lazy('order_detail', kwargs={'pk': self.kwargs.get('order_pk')})

    def get_title(self):
        return _('Deletion of an offer to order %s') % self.get_object().order.title

    def get_btn_value(self):
        return _('Delete')
