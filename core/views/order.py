import json

from django.contrib import messages
from django.core.paginator import Paginator
from django.db.models import Q
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.urls import reverse_lazy, reverse, resolve
from django.utils import timezone
from django.views import generic
from django.http import response, Http404
from django.views.generic import CreateView
from django.utils.translation import ugettext_lazy as _

from core import models
from core import forms
from core.forms import OfferModelForm, MessageModelForm, Message, FeedbackForm
from core.models import SubCategoryField, SubCategoryFieldValue
from core.views import core_generic
from core.views.other import was_read


class OrderCreateFieldsMixin:
    sub_field_ids = list()
    sub_field_values = dict()

    def post(self, request, *args, **kwargs):
        self.request.POST = self.request.POST.copy()
        if self.request.POST.get('sub_field_id'):
            self.sub_field_ids = self.request.POST.pop('sub_field_id')

            for field_id in self.sub_field_ids:
                value_key = 'value_' + field_id
                self.sub_field_values.update({value_key: self.request.POST.pop(value_key)})
        return super().post(request, *args, **kwargs)

    def save_field(self, order):
        for field_id in self.sub_field_ids:
            value_key = 'value_' + field_id
            value = self.sub_field_values.get(value_key)

            if SubCategoryField.objects.get(id=field_id).field_type != SubCategoryField.MULTIPLE_CHOICE_FIELD:
                value = value[0] if len(value) == 1 else value
            form = forms.SubCategoryFieldForm(data={value_key: value, 'sub_field_id': field_id})

            if form.is_valid():
                field = models.SubCategoryField.objects.get(id=field_id)
                if isinstance(value, list):
                    value = ','.join(value)
                models.SubCategoryFieldValue.objects.update_or_create(order=order, field=field,
                                                                      defaults={'value': value})

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form, request_post=dict(self.request.POST)))


class OrderCreate(OrderCreateFieldsMixin, generic.CreateView):
    model = models.Order
    form_class = forms.Order
    template_name = 'core/order/edit.html'

    def get_initial(self):
        initial = super().get_initial()
        initial['category'] = self.request.GET.get('category')
        initial['currency'] = self.request.user.currency
        return initial

    def form_valid(self, form):
        if not self.request.POST.get('save_as_bookmark'):
            self.object = self.model.objects.create(**form.cleaned_data, owner=self.request.user,
                                                    published_at=timezone.now(), was_published=True)
            self.object.send_potential(self.request)
        else:
            self.object = self.model.objects.create(**form.cleaned_data, owner=self.request.user)
        self.save_field(self.object)
        return response.HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse_lazy('order_detail', kwargs={'pk': self.object.pk})


class OrderUpdate(OrderCreateFieldsMixin, generic.UpdateView):
    model = models.Order
    form_class = forms.Order
    template_name = 'core/order/edit.html'

    def get(self, request, *args, **kwargs):
        if self.get_object().winner:
            return redirect(reverse_lazy('order_detail', kwargs={'pk': self.get_object().pk}))
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        return self.model.objects.filter(owner=self.request.user)

    def form_valid(self, form):
        if not self.request.POST.get('save_as_bookmark'):
            self.object = form.save(commit=False)
            self.object.was_published = True
            self.object.published_at = timezone.now()
            self.object.save()
            self.object.send_potential(self.request)
        else:
            self.object = form.save(commit=True)
        self.save_field(self.object)
        return response.HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse_lazy('order_detail', kwargs={'pk': self.object.pk})


class OrderDetail(core_generic.DetailView):
    model = models.Order
    pk_url_kwarg = 'pk'
    template_name = 'core/order/detail.html'
    context_object_name = 'order'
    dialogs_paginate_by = 2

    def get(self, request, *args, **kwargs):
        try:
            order = models.Order.objects.get(pk=self.kwargs.get('pk'))
        except Exception as e:
            raise Http404

        if request.user.is_anonymous and order.only_for_registered:
            return redirect(reverse('login') + '?next=%s' % request.path)

        if order.get_status() == 'Draft' and order.owner_id != request.user.id:
            return redirect(reverse('order_list'))
        return super().get(request, *args, **kwargs)

    def post(self, *args, **kwargs):
        self.save_offer()

        return redirect(self.request.path)

    def get_provider_offer(self):
        try:
            return self.get_object().offers.get(user=self.request.user, is_declined=False)
        except Exception as e:
            return None

    def get_offer_form(self):
        return forms.OfferModelForm(self.request.POST or None, order=self.get_object())

    def get_message_form(self):
        return forms.Message(self.request.POST or None)

    def get_additional_fields(self):
        return SubCategoryFieldValue.objects.filter(order=self.get_object()).select_related('field')

    def save_offer(self):
        form = self.get_offer_form()
        if form.is_valid():
            order = self.get_object()
            offer = form.save(commit=False)
            offer.order = order
            offer.file = self.request.FILES.get('file')
            offer.user = self.request.user
            offer.save()
            models.Message.objects.create(recipient=order.owner, sender=self.request.user, text=offer.comment, order=order)
            messages.success(self.request, _('The response was successfully created'))

            order_respond_template = render_to_string('core/emails/order_respond.%s.html' %
                                                      self.request.LANGUAGE_CODE, {'order_id': order.pk})
            order.owner.email_user(subject=_('New offer has arrived'), html_message=order_respond_template)

    def get_dialogs(self):
        order = self.get_object()
        user = self.request.user
        dialogs = dict()
        mess = models.Message.objects.filter(Q(sender=user) | Q(recipient=user), order=order)
        was_read(mess, user)

        if user == order.owner:
            for message in mess:
                key = message.sender if message.sender != order.owner else message.recipient
                if not dialogs.get(key):
                    dialogs[key] = list()
                dialogs[key].append(message)
        else:
            dialogs[order.owner] = mess

        for key, mess in dialogs.items():
            dialogs[key] = list(reversed(mess[:3]))
        return dialogs

    def get_dialogs_paginator(self):
        dialogs = self.get_dialogs()
        dialogs = tuple(dialogs.items())
        paginator = Paginator(dialogs, per_page=self.dialogs_paginate_by)
        return paginator

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['additional_fields'] = self.get_additional_fields()

        if self.request.user.is_authenticated:
            page = self.request.GET.get('messages_page')
            context['message_form'] = self.get_message_form()
            context['provider_offer'] = self.get_provider_offer()
            context['offer_form'] = self.get_offer_form()

            dialogs_paginator = self.get_dialogs_paginator()
            context['dialogs'] = dialogs_paginator.page(page if page in dialogs_paginator.page_range else 1).object_list
            context['dialogs_paginator'] = dialogs_paginator
        return context


class OrderList(core_generic.ListView):
    queryset = models.Order.get_actual()
    template_name = 'core/order/list.html'
    paginate_by = 30
    context_object_name = 'orders'

    def get_queryset(self):
        qst = super().get_queryset().filter(Q(offer_deadline__gt=timezone.now()) | Q(offer_deadline__isnull=True),
                                            was_published=True)

        if self.request.user.is_anonymous:
            qst = qst.exclude(only_for_registered=True)
        return self.filter_queryset(qst)

    def filter_queryset(self, qst):
        request = self.request

        if request.GET.get('potential_requests'):
            service_subs = [service.subcategory for service in
                            models.UserService.objects.filter(user=self.request.user)]
            qst = qst.filter(subcategory__in=service_subs)
        if request.GET.get('category'):
            qst = qst.filter(category=request.GET.get('category'))
        if request.GET.get('subcategory'):
            qst = qst.filter(subcategory=request.GET.get('subcategory'))
        return qst

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter_form'] = forms.OrderFilterForm(self.request.GET or None, user=self.request.user)
        return context


class UserOrderList(core_generic.ListView):
    model = models.Order
    template_name = 'core/order/user_order_list.html'
    paginate_by = 30

    def get_queryset(self):
        return super().get_queryset().filter(owner=self.request.user)

    def get_title(self):
        return _('My requests')


class OrderDeleteView(core_generic.DeleteView):
    model = models.Order
    template_name = 'core/cun/notification.html'
    success_url = reverse_lazy('my_order_list')
    pk_url_kwarg = 'pk'

    def get(self, request, *args, **kwargs):
        if self.get_object().winner:
            return redirect(reverse_lazy('order_detail', kwargs={'pk': self.get_object().id}))
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        return self.model.objects.filter(owner=self.request.user)

    def get_title(self):
        return 'Вы действительно хотите удалить заявку %s?' % self.get_object().title


class OrderFeedbackCreateView(CreateView):
    model = models.Feedback
    form_class = FeedbackForm
    template_name = 'core/order/feedback.html'
    _order = None

    def get(self, request, *args, **kwargs):
        order = self.get_order()
        user = request.user

        if order.owner == user and not order.has_performer_feedback() or \
                order.offer.user == user and not order.has_customer_feedback():
            return super().get(request, *args, **kwargs)
        return redirect(reverse('order_detail', kwargs={'pk': order.id}))

    def get_order(self):
        if not self._order:
            self._order = models.Order.objects.get(id=self.kwargs.get('pk'))
        return self._order

    def get_title(self):
        order = self.get_order()
        if self.request.user == order.owner:
            recipient = order.offer.user
            title = _('Leave a feedback about the work of the provider %s') % recipient
        else:
            recipient = order.owner
            title = _('Leave a feedback about the customer %s') % recipient
        return title

    def form_valid(self, form):
        feedback = form.save(commit=False)
        order = self.get_order()
        feedback.order = order

        if self.request.user == order.owner:
            feedback.created_by = order.owner
            feedback.recipient = order.offer.user
            subject = _('Customer has rate your company')
        else:
            feedback.created_by = order.offer.user
            feedback.recipient = order.owner
            subject = _('Provider has rate your company')

        context = dict(
            order_id=order.id,
            created_by=feedback.created_by,
            recipient=feedback.recipient,
            ps=not order.feedbacks.filter(order=order, recipient=feedback.created_by),
        )

        order_feedback_template = render_to_string('core/emails/order_feedback.%s.html' %
                                                   self.request.LANGUAGE_CODE, context)
        feedback.recipient.email_user(subject=subject, html_message=order_feedback_template)

        feedback.update_data()
        feedback.save()
        feedback.recipient.set_rating()
        return redirect(reverse_lazy('order_detail', kwargs={'pk': self.kwargs.get('pk')}))

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['title'] = self.get_title()
        return context
