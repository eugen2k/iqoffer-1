import datetime
import json
import time
from urllib import parse

from django.contrib import messages
from django.shortcuts import redirect, render
from django.template.loader import render_to_string
from django.utils.timezone import now
from django.urls import reverse
from django.views import generic
from django.utils.translation import ugettext_lazy as _

from project.settings import SERVER_HOSTNAME
from project.settings import CAPTCHA_KEY, CAPTCHA_SECRET_KEY

from core.forms import PreUserInviteForm
from core.models import PreUser, Slider, SubcategoryPreview


class Index(generic.TemplateView):
    template_name = None

    def get_template_names(self):
        return ['core/index.%s.html' % self.request.LANGUAGE_CODE]

    def post(self, request, *args, **kwargs):
        form = PreUserInviteForm(request.POST or None, request=request)
        if form.is_valid():
            perm = form.cleaned_data.pop('perm', None)
            sender_email = form.cleaned_data.get('from_email')
            recipient_email = form.cleaned_data.get('email')

            pre_user = PreUser.objects.filter(email=recipient_email)

            if pre_user.filter(from_email=sender_email):
                messages.add_message(self.request, messages.WARNING,
                                     _("You've already sent an invitation to such email"))
            elif pre_user:
                pre_user = pre_user.first()
                d2 = time.mktime(now().timetuple())
                d1 = time.mktime(pre_user.send_at.timetuple())
                minutes_passed = int(d2 - d1) / 60
                if minutes_passed >= 30:
                    pre_user = PreUser.objects.create(**form.cleaned_data, **{perm: True}, send_at=now(),
                                                      from_site=True)
                    self.invite(pre_user)
                else:
                    messages.add_message(self.request, messages.WARNING,
                                         _("Someone has already sent an invitation to this email less than 30 minutes ago"))
            else:
                pre_user = PreUser.objects.create(**form.cleaned_data, **{perm: True}, send_at=now(), from_site=True)
                self.invite(pre_user)
            return redirect(reverse('index'))
        else:
            errors = json.loads(form.errors.as_json()).get('__all__')
            for error in errors:
                message = error.get('message')
                messages.add_message(self.request, messages.WARNING, message)
            return self.render_to_response(self.get_context_data(invite_form=form))

    def invite(self, pre_user):
        registration_url_parts = list(parse.urlparse(parse.urljoin(SERVER_HOSTNAME, reverse('registration'))))
        registration_url_parts[4] = parse.urlencode(dict(invite=str(pre_user.token)))
        invite_url = parse.urlunparse(registration_url_parts)

        invite_template = render_to_string('core/emails/pre_user_index_invite.%s.html' %
                                           self.request.LANGUAGE_CODE, {'invite_url': invite_url, 'user': pre_user})
        pre_user.email_user(subject=_('Invite to iqOffer'), html_message=invite_template)

        message = _('Invite to %s successfully sent') % pre_user.email

        messages.add_message(self.request, messages.SUCCESS, message)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        initial = {'from_email': user.email, 'from_username': user.username} if user.is_authenticated else {}
        context['invite_form'] = PreUserInviteForm(self.request.POST or None, initial=initial, request=self.request)
        context['sliders'] = Slider.objects.filter(show_on_website=True, language=self.request.LANGUAGE_CODE)
        context['orders'] = SubcategoryPreview.objects.filter(lang=self.request.LANGUAGE_CODE)
        context['captcha_key'] = CAPTCHA_KEY
        context['captcha_secret_key'] = CAPTCHA_SECRET_KEY
        return context
