from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _

from core.views import core_generic
from core.models import New
from core.forms import NewForm


class NewListView(core_generic.ListView):
    model = New
    template_name = 'core/news/list.html'
    context_object_name = 'news'
    paginate_by = 10

    def get_title(self):
        return _('News')

    def get_queryset(self):
        return self.model.objects.filter(language=self.request.LANGUAGE_CODE)


class NewDetailView(core_generic.DetailView):
    model = New
    template_name = 'core/news/detail.html'
    context_object_name = 'new'
    slug_url_kwarg = 'slug'

    def get_title(self):
        return self.object.title

    def get_queryset(self):
        return self.model.objects.filter(language=self.request.LANGUAGE_CODE)
