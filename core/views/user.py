from django.core.paginator import Paginator
from django.db.models import Q
from django.shortcuts import redirect, render
from django.template.loader import render_to_string
from django.urls import reverse, reverse_lazy
from django.utils import translation
from django.views import generic
from core.views import core_generic
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _

from core import models
from core import forms
from core.views.other import was_read


class Profile(generic.DetailView):
    model = models.User
    template_name = 'core/user/profile.html'
    pk_url_kwarg = 'pk'
    context_object_name = 'user'
    _user = None
    _user_feedbacks = None

    def get_user(self):
        if not self._user:
            self._user = models.User.objects.get(id=self.kwargs['pk'])
        return self._user

    def get_paginator_feedbacks(self):
        feedbacks_list = self.get_user_feedbacks()
        paginator = Paginator(feedbacks_list, 2)
        page = self.request.GET.get('page') or 1
        feedbacks = paginator.page(page)
        return feedbacks

    def get_user_feedbacks(self):
        if not self._user_feedbacks:
            user = self.get_user()
            self._user_feedbacks = user.feedbacks_received.all()
        return self._user_feedbacks

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.get_user()
        user_feedbacks = self.get_user_feedbacks()

        context['feedbacks'] = self.get_paginator_feedbacks()
        context['positive_feedbacks'] = user_feedbacks.filter(is_recommended=True).count()
        context['negative_feedbacks'] = user_feedbacks.filter(is_recommended=False).count()
        context['can_see_contacts'] = user.can_see_contacts(self.request.user)
        user = self.request.user
        if self.request.user.is_authenticated:
            context['orders'] = models.Order.objects.filter(Q(owner=user) | Q(winner=user))
        return context


class UserServiceList(core_generic.ListView):
    model = models.UserService
    template_name = 'core/services/list.html'
    paginate_by = 30
    context_object_name = 'services'

    def filter_queryset(self, qst):
        user_pk = self.kwargs.get('pk')
        request = self.request

        if self.request.GET.get('country'):
            qst = qst.filter(country=self.request.GET.get('country'))
        if self.request.GET.get('city'):
            qst = qst.filter(city=self.request.GET.get('city'))
        if self.request.GET.get('category'):
            qst = qst.filter(category=self.request.GET.get('category'))
        if self.request.GET.get('subcategory'):
            qst = qst.filter(subcategory=self.request.GET.get('subcategory'))
        if self.request.GET.get('business_category'):
            qst = qst.filter(business_category=self.request.GET.get('business_category'))
        if self.request.GET.get('my_business_category'):
            qst = qst.filter(business_category=self.request.user.business_category)

        if request.GET.get('service_name'):
            qst = qst.filter(name__icontains=request.GET.get('service_name'))
        if self.for_specific_user and user_pk:  # TODO нах это надо вообще?
            qst = qst.filter(user=self.request.user)
        return qst

    def __init__(self, **kwargs):
        self.for_specific_user = False
        super().__init__(**kwargs)

    def get_queryset(self):
        qst = super().get_queryset().exclude(user__isnull=True).order_by('-user__rating').select_related('user')
        return self.filter_queryset(qst)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter_form'] = forms.ServiceFilterForm(self.request.GET or None, user=self.request.user)
        return context


class UserServiceDetail(core_generic.DetailView):
    model = models.UserService
    pk_url_kwarg = 'pk'
    template_name = 'core/services/detail.html'
    context_object_name = 'service'
    dialogs_paginate_by = 2

    def get_message_form(self):
        return forms.Message(self.request.POST or None)

    def get_dialogs(self):
        service = self.get_object()
        user = self.request.user
        dialogs = dict()
        mess = models.Message.objects.filter(Q(sender=user) | Q(recipient=user), service=service)
        was_read(mess, user)
        if user == service.user:
            for message in mess:
                key = message.sender if message.sender != service.user else message.recipient
                if not dialogs.get(key):
                    dialogs[key] = list()
                dialogs[key].append(message)
        else:
            dialogs[service.user] = mess

        for key, mess in dialogs.items():
            dialogs[key] = list(reversed(mess[:3]))

        return dialogs

    def get_dialogs_paginator(self):
        dialogs = self.get_dialogs()
        dialogs = tuple(dialogs.items())
        paginator = Paginator(dialogs, per_page=self.dialogs_paginate_by)
        return paginator

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            page = self.request.GET.get('messages_page')
            context['message_form'] = self.get_message_form()
            dialogs_paginator = self.get_dialogs_paginator()
            context['dialogs'] = dialogs_paginator.page(page if page in dialogs_paginator.page_range else 1).object_list
            context['dialogs_paginator'] = dialogs_paginator
        return context


class InviteUserToOrderCreateView(core_generic.CreateView):
    model = models.Invite
    template_name = 'core/cun/create_or_update.html'
    form_class = forms.InviteUserToOrderForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['orders'] = models.Order.get_actual(qs=models.Order.objects.filter(owner=self.request.user))
        return kwargs

    def form_valid(self, form, commit=True):
        order = form.cleaned_data.get('order')
        obj = form.save(commit=False)
        service = models.UserService.objects.get(id=self.kwargs['pk'])
        obj.user = order.owner
        obj.order = order
        obj.service = service
        obj.save()

        with translation.override(service.user.selected_lang):
            perform_template = render_to_string('core/emails/perform_request.%s.html' %
                                                service.user.selected_lang,
                                                {'order_id': order.pk, 'service_id': service.pk})
            service.user.email_user(subject=_('Invitation to perform a request'), html_message=perform_template)

        messages.add_message(self.request, messages.SUCCESS,
                             _('Invitation to participate in request "{order}" successfully sent to {user}').format(
                                 **{'user': service.user, 'order': order}))
        return redirect(reverse('full_service_list'))

    def get_btn_value(self):
        return _('Invite')


class CustomerServiceResponseList(core_generic.ListView):
    model = models.UserService
    paginate_by = 30
    template_name = 'core/services/customer_responses.html'
    context_object_name = 'services'

    def get(self, request, *args, **kwargs):
        if request.user.is_customer:
            return super().get(request, *args, **kwargs)
        return redirect('index')

    def get_queryset(self):
        return self.model.objects.filter(messages__sender=self.request.user).distinct().order_by('messages__isread')

    def get_title(self):
        return _('Services responses')
