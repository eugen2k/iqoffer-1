import datetime
from collections import OrderedDict
from urllib.parse import unquote

from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import translate_url
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.utils.http import is_safe_url
from django.utils.translation import LANGUAGE_SESSION_KEY, check_for_language

from core.models import Faq, Terms, Privacy, Country, PreUser, Order, Category, SubCategory, Offer, City, \
    SubCategoryFieldValue, Tariff

from core.views import core_generic

def change_language(request):
    lang_code = request.GET.get('lang')

    next = request.POST.get('next', request.GET.get('next'))
    if not next:
        next = request.META.get('HTTP_REFERER')
        if next:
            next = unquote(next)
        else:
            next = '/'

    response = HttpResponseRedirect(next) if next else HttpResponse(status=204)

    if lang_code and check_for_language(lang_code):
        if next:
            next_trans = translate_url(next, lang_code)
            if next_trans != next:
                response = HttpResponseRedirect(next_trans)
        if hasattr(request, 'session'):
            request.session[LANGUAGE_SESSION_KEY] = lang_code
        else:
            response.set_cookie(
                settings.LANGUAGE_COOKIE_NAME, lang_code,
                max_age=settings.LANGUAGE_COOKIE_AGE,
                path=settings.LANGUAGE_COOKIE_PATH,
                domain=settings.LANGUAGE_COOKIE_DOMAIN,
            )

    user = request.user
    if user.is_authenticated:
        user.selected_lang = lang_code
        user.save()
    return response


class TariffListView(core_generic.ListView):
    template_name = 'core/tariff/list.html'
    model = Tariff
    context_object_name = 'tariffs'

    def get_queryset(self):
        return self.model.objects.filter(language=self.request.LANGUAGE_CODE).order_by('-position')


def faq(request):
    context = {
        'faqs': Faq.objects.filter(language=request.LANGUAGE_CODE).order_by('position')
    }
    return render(request, 'core/faq.html', context)


def terms(request):
    context = {
        'term': Terms.objects.filter(language=request.LANGUAGE_CODE).first()
    }
    return render(request, 'core/terms.html', context)


def privacy(request):
    context = {
        'privacy': Privacy.objects.filter(language=request.LANGUAGE_CODE).first()
    }
    return render(request, 'core/privacy.html', context)


def dashboard(request):
    days_yearly = timezone.now() - datetime.timedelta(days=7)
    orders_countries_responses = OrderedDict()
    orders_cities_responses = OrderedDict()
    categories_subcategories_data = OrderedDict()
    hosting_configurations = OrderedDict()

    successful_admin_invites = PreUser.objects.filter(from_site=False, user__isnull=False, show_in_stat=True)
    successful_site_invites = PreUser.objects.filter(from_site=True, user__isnull=False, show_in_stat=True)

    waiting_admin_invites = PreUser.objects.filter(from_site=False, user__isnull=True, created_at__gt=days_yearly,
                                                   show_in_stat=True)
    waiting_site_invites = PreUser.objects.filter(from_site=True, user__isnull=True, created_at__gt=days_yearly,
                                                  show_in_stat=True)

    failed_admin_invites = PreUser.objects.filter(from_site=False, user__isnull=True, show_in_stat=True).exclude(
        id__in=waiting_admin_invites)
    failed_site_invites = PreUser.objects.filter(from_site=True, user__isnull=True, show_in_stat=True).exclude(id__in=waiting_site_invites)

    for country in Country.objects.all():
        orders_countries_responses[country] = Offer.objects.filter(
            order__in=Order.objects.filter(owner__country=country)).count()
    for city in City.objects.all():
        orders_cities_responses[city] = Offer.objects.filter(order__in=Order.objects.filter(owner__city=city)).count()

    for subcategory in SubCategory.objects.all():
        orders = subcategory.orders.filter(was_published=True)
        average_lifetime = datetime.timedelta(days=0, hours=0, minutes=0)

        if not orders:
            continue

        for order in orders:
            order_avg_lifetime = None
            if order.offer:
                order_avg_lifetime = order.offer.created_at - order.published_at
            elif order.offer_deadline and timezone.now() >= order.offer_deadline:
                order_avg_lifetime = order.offer_deadline - order.published_at

            if order_avg_lifetime:
                average_lifetime += order_avg_lifetime

        average_lifetime = average_lifetime.total_seconds() / orders.count()

        days = average_lifetime // 86400
        hours = (average_lifetime - days * 86400) // 3600
        hours += round((average_lifetime - days * 86400 - hours * 3600) // 60 / 60, 2)

        average_lifetime = dict(days=int(days), hours=hours)
        if average_lifetime.get('days') or average_lifetime.get('hours'):
            average_lifetime = '{days} days, {hours} hours'.format(**average_lifetime)
        else:
            average_lifetime = 'No requests closed'

        key = '%s/%s' % (_(subcategory.category.name), _(subcategory.name))
        categories_subcategories_data[key] = dict()
        categories_subcategories_data[key]['budgets_from'] = orders.values_list('budget_from', flat=True)
        categories_subcategories_data[key]['budgets_to'] = orders.values_list('budget_to', flat=True)
        categories_subcategories_data[key]['average_lifetime'] = average_lifetime

    for field_value in SubCategoryFieldValue.objects.filter(order__category__name='Hosting'):
        if not hosting_configurations.get(field_value.order):
            hosting_configurations[field_value.order] = list()
        hosting_configurations[field_value.order].append((_(field_value.field.label), field_value.value))

    """2) количество заявок открытых; закрытых (успешных) и всего"""
    total_orders_count = Order.objects.count()
    draft_orders = Order.get_draft_orders()
    successful_orders = Order.get_successful_orders()
    fail_orders = Order.get_fail_orders()
    published_orders = Order.get_published_orders()

    context = {
        'title': _('Dashboard'),

        'successful_admin_invites': successful_admin_invites,
        'successful_site_invites': successful_site_invites,
        'waiting_admin_invites': waiting_admin_invites,
        'waiting_site_invites': waiting_site_invites,
        'failed_admin_invites': failed_admin_invites,
        'failed_site_invites': failed_site_invites,

        'total_orders_count': total_orders_count,
        'draft_orders': draft_orders,
        'successful_orders': successful_orders,
        'fail_orders': fail_orders,
        'published_orders': published_orders,

        'orders_countries_responses': orders_countries_responses,
        'orders_cities_responses': orders_cities_responses,

        'hosting_configurations': hosting_configurations,

        'categories_subcategories_data': categories_subcategories_data,
        'total_categories_orders': Category.objects.exclude(orders__isnull=True).count(),
        'total_subcategories_orders': SubCategory.objects.exclude(orders__isnull=True).count()
    }
    return render(request, 'core/dashboard.html', context)


def was_read(messages, recipient):
    for message in messages:
        if recipient == message.recipient:
            message.is_read = True
            message.save()
