import datetime

from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.views.generic import UpdateView, ListView as DjangoListView, DetailView as DjangoDetailView, \
    View as DjangoView, TemplateView as DjangoTemplateView, DeleteView as DjangoDeleteView, FormView as DjangoFormView, \
    CreateView as DjangoCreateView


# from core.datatools import get_select2_options


class TitleMixin(object):
    title = ''

    def get_title(self):
        return self.title

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.get_title()
        return context


class SeoMixin(object):
    keywords = None
    description = None

    def separate_title(self):
        title = self.get_title()
        return ','.join((title.split()))

    def get_keywords(self):
        return self.keywords or self.separate_title()

    def get_description(self):
        return self.description or self.get_title()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['meta_keywords'] = self.get_keywords()
        context['meta_description'] = self.get_description()
        return context


class Select2OptionsMixin(object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # if 'form' in context:
        #     model_instanse = self.get_object() if hasattr(self, 'get_object') else None
        #     context['select2_options'] = get_select2_options(context['form'], model_instanse)
        return context


class BtnMixin:
    def get_btn_value(self):
        pass

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['btn_value'] = self.get_btn_value()
        return context


class CreateOrUpdateView(SeoMixin, Select2OptionsMixin, BtnMixin, TitleMixin, UpdateView):
    http_method_names = ['get', 'post']
    form_class_create = None
    form_class_update = None
    _object = None

    def is_update(self):
        return bool(self.get_object().id)

    def is_create(self):
        return not self.is_update()

    def get_form_class(self):
        if self.is_create():
            self.form_class = self.form_class_create
        elif self.is_update():
            self.form_class = self.form_class_update
        return super(CreateOrUpdateView, self).get_form_class()

    def form_valid(self, form, commit=True):
        self.object = form.save(commit=commit)
        return redirect(self.get_success_url())

    def _get_object_from_qs(self, queryset):
        return queryset.get()

    def _init_object(self):
        return self.model()

    def get_delete_url(self):
        return None

    def get_object(self, queryset=None):
        if not self._object:
            if queryset is None:
                queryset = self.get_queryset()

            pk = self.kwargs.get(self.pk_url_kwarg, None)
            slug = self.kwargs.get(self.slug_url_kwarg, None)

            if pk or slug:
                if pk is not None:
                    queryset = queryset.filter(pk=pk)

                elif slug is not None:
                    slug_field = self.get_slug_field()
                    queryset = queryset.filter(**{slug_field: slug})

                self._object = self._get_object_from_qs(queryset)
            else:
                self._object = self._init_object()
        return self._object

    def get_context_data(self, **kwargs):
        context = super(CreateOrUpdateView, self).get_context_data(**kwargs)
        context['is_update'] = self.is_update()
        context['is_create'] = self.is_create()
        if self.is_update():
            context['delete_url'] = self.get_delete_url()
        return context


class DeleteView(SeoMixin, TitleMixin, DjangoDeleteView):
    _object = None

    def _init_object(self):
        return self.model()

    def _get_object_from_qs(self, queryset):
        return queryset.get()

    def get_object(self, queryset=None):
        if not self._object:
            if queryset is None:
                queryset = self.get_queryset()

            pk = self.kwargs.get(self.pk_url_kwarg, None)
            slug = self.kwargs.get(self.slug_url_kwarg, None)

            if pk or slug:
                if pk is not None:
                    queryset = queryset.filter(pk=pk)

                elif slug is not None:
                    slug_field = self.get_slug_field()
                    queryset = queryset.filter(**{slug_field: slug})

                self._object = self._get_object_from_qs(queryset)
            else:
                self._object = self._init_object()
        return self._object

    def get_context_data(self, **kwargs):
        context = super(DeleteView, self).get_context_data(**kwargs)
        context['success_url'] = self.get_success_url()
        return context

    def delete(self, request, *args, **kwargs):
        if hasattr(self, 'deleted_at'):
            self.object = self.get_object()
            self.object.deleted_at = datetime.datetime.now()
            self.object.save()
        else:
            super().delete(request, *args, **kwargs)
        return HttpResponseRedirect(self.get_success_url())


class ListView(SeoMixin, TitleMixin, DjangoListView):
    pass


class CreateView(SeoMixin, Select2OptionsMixin, BtnMixin, TitleMixin, DjangoCreateView):
    pass


class DetailView(SeoMixin, TitleMixin, DjangoDetailView):
    pass


class FormView(SeoMixin, TitleMixin, DjangoFormView):
    pass


class View(SeoMixin, TitleMixin, DjangoView):
    pass


class TemplateView(SeoMixin, Select2OptionsMixin, TitleMixin, DjangoTemplateView):
    pass
