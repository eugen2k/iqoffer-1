import json
import re

from urllib import parse
from django.http import JsonResponse, HttpResponse
from django.template.loader import render_to_string
from django.forms.models import model_to_dict
from django.utils import translation
from django.utils.translation import ugettext_lazy as _

from core.forms import SubCategoryFieldForm
from core.models import SubCategory, SubCategoryField, Order, SubCategoryFieldValue, Notification, Message, Country, \
    City, Category


def get_categories_selector(request):
    print(request.LANGUAGE_CODE)
    categories = [{'id': category.id, 'name': _(category.name)} for category in Category.objects.all()]
    subcategories = [{'id': subcategory.id, 'name': _(subcategory.name), 'category_id': subcategory.category.id} for
                     subcategory in SubCategory.objects.all()]

    template = render_to_string('core/ajax/categories_selector.html',
                                {'categories': categories, 'subcategories': subcategories})
    return JsonResponse({'template': template, 'title': _('Select category & subcategory')})


def get_sub_categories(request):
    category_id = request.GET.get('category_id')
    sub_categories = []
    if category_id:
        sub_categories = SubCategory.objects.filter(category_id=category_id)
    return JsonResponse({'sub_categories': [{'id': sub_category.id, 'name': _(sub_category.name)} for sub_category in
                                            sub_categories]})


def get_initial_subcategory_fields(request):
    if not request.GET['sub_id']:
        return JsonResponse({'fields': []})
    sub_category = SubCategory.objects.get(id=request.GET['sub_id'])
    fields = SubCategoryField.objects.filter(sub_category=sub_category, language=request.LANGUAGE_CODE)
    field_forms = list()
    order = None
    if request.GET.get('order_id'):
        order = Order.objects.filter(id=request.GET.get('order_id')).first()

    for field in fields:
        value = 0
        try:
            value = SubCategoryFieldValue.objects.get(field=field, order=order).value
            value_key = 'value_' + str(field.id)
            if field.field_type == field.MULTIPLE_CHOICE_FIELD:
                value = value.split(',')
            form = SubCategoryFieldForm(data={value_key: value, 'sub_field_id': field.id})
        except Exception as e:
            form = SubCategoryFieldForm(field_id=field.id)
        field_forms.append(render_to_string('core/ajax/sub_category_field.html', context={'form': form, 'field': field,
                                                                                          'value': value}))
    return JsonResponse({'fields': field_forms})


def get_bound_subcategory_fields(request):
    field_forms = []
    post_data = json.loads(request.GET.get('post_data'))
    if not post_data.get('sub_field_id'):
        return JsonResponse({'fields': []})

    field_ids = list(re.sub("[^0-9]", "", post_data.get('sub_field_id').replace('&#39;', '')))
    for field_id in field_ids:
        try:
            field = SubCategoryField.objects.get(id=field_id, language=request.LANGUAGE_CODE)

            value = json.loads(post_data.get('value_%s' % field.id).replace('&#39;', '"')[1:-1])
            value = value[0] if len(value) == 1 else value
            value_key = 'value_' + str(field.id)

            form = SubCategoryFieldForm(data={value_key: value, 'sub_field_id': field.id})
            field_forms.append(render_to_string('core/ajax/sub_category_field.html', context={'form': form}))
        except Exception as e:
            pass
    return JsonResponse({'fields': field_forms})


def get_notifications_count(request):
    return JsonResponse({'count': Notification.objects.filter(recipient=request.user, is_read=False).count()})


def set_read_to_notifications(request):
    Notification.objects.filter(recipient=request.user, is_read=False).update(is_read=True)
    return HttpResponse(status=200)


def get_messages_count(request):
    if request.user.is_anonymous:
        return HttpResponse(status=403)
    return JsonResponse({'count': Message.objects.filter(recipient=request.user, is_read=False).count()})


def get_cities(request):
    country_id = request.GET.get('country_id')
    cities = list()
    cities.append({'id': '', 'name': '----------'})
    if country_id:
        for city in City.objects.filter(country_id=country_id):
            city = model_to_dict(city)
            city['name'] = _(city['name'])
            cities.append(city)
    return JsonResponse({'cities': cities})
