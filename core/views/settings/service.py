from django.views import generic
from django.http import response
from django.urls import reverse_lazy

from core import models
from core import forms


class ServiceCreate(generic.CreateView):
    model = models.UserService
    form_class = forms.UserService
    template_name = 'core/settings/service/edit.html'
    success_url = reverse_lazy('service_list')

    def get_initial(self):
        initial = super().get_initial()
        initial['category'] = self.request.GET.get('category')
        initial['subcategory'] = self.request.GET.get('subcategory')
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_invalid(self, form):
        return super(ServiceCreate, self).form_invalid(form)

    def form_valid(self, form):
        self.object = self.model.objects.create(**form.cleaned_data, user=self.request.user)
        return response.HttpResponseRedirect(self.get_success_url())


class ServiceList(generic.ListView):
    model = models.UserService
    template_name = 'core/settings/service/list.html'
    paginate_by = 20

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user)


class ServiceUpdate(generic.UpdateView):
    form_class = forms.UserService
    model = models.UserService
    template_name = 'core/settings/service/edit.html'
    success_url = reverse_lazy('service_list')

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user)


class ServiceDelete(generic.DeleteView):
    model = models.UserService
    template_name = 'core/settings/service/delete.html'
    success_url = reverse_lazy('service_list')

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user)
