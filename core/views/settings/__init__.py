from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views import generic
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _

from core import models
from core import forms


class SettingsMixin:
    def get_success_url(self):
        return self.request.path

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):
        messages.success(self.request, _("Saved successfully"))
        obj = form.save(commit=False)

        try:
            pre_user = models.PreUser.objects.get(user=self.request.user)
        except models.PreUser.DoesNotExist:
            pre_user = None

        obj.pre_user = pre_user
        obj.save()
        return redirect(self.get_success_url())


class Requisites(SettingsMixin, generic.UpdateView):
    model = models.Requisites
    form_class = forms.Requisites
    template_name = 'core/settings/requisites.html'

    def get_object(self, queryset=None):
        return self.model.objects.get_or_create(user=self.request.user)[0]

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['language'] = self.request.LANGUAGE_CODE
        return kwargs


class Profile(SettingsMixin, generic.UpdateView):
    model = models.User
    form_class = forms.UserProfile
    template_name = 'core/settings/profile.html'


class Contact(SettingsMixin, generic.UpdateView):
    model = models.UserContact
    form_class = forms.UserContact
    template_name = 'core/settings/contact.html'

    def get_object(self, queryset=None):
        return self.model.objects.get_or_create(user=self.request.user)[0]

    def form_valid(self, form):
        if self.request.POST.get('save_and_continue'):
            form.save(commit=True)
            return redirect(reverse_lazy('order_list'))
        return super().form_valid(form)

