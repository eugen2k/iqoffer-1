from django.views import generic
from django.http import response
from django.urls import reverse_lazy

from core import models
from core import forms


class OfficeCreate(generic.CreateView):
    model = models.Office
    form_class = forms.Office
    template_name = 'core/settings/office/edit.html'
    success_url = reverse_lazy('office_list')

    def form_valid(self, form):
        self.object = self.model.objects.create(**form.cleaned_data, user=self.request.user)
        return response.HttpResponseRedirect(self.get_success_url())


class OfficeList(generic.ListView):
    model = models.Office
    template_name = 'core/settings/office/list.html'

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user)


class OfficeUpdate(generic.UpdateView):
    form_class = forms.Office
    model = models.Office
    template_name = 'core/settings/office/edit.html'
    success_url = reverse_lazy('office_list')

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user)


class OfficeDelete(generic.DeleteView):
    model = models.Office
    template_name = 'core/settings/office/delete.html'
    success_url = reverse_lazy('office_list')

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user)
