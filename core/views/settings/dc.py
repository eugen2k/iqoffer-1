from django.views import generic
from django.http import response
from django.urls import reverse_lazy

from core import models
from core import forms


class DCCreate(generic.CreateView):
    model = models.DataCenter
    form_class = forms.DataCenter
    template_name = 'core/settings/data_center/edit.html'
    success_url = reverse_lazy('dc_list')

    def form_valid(self, form):
        self.object = self.model.objects.create(**form.cleaned_data, user=self.request.user)

        return response.HttpResponseRedirect(self.get_success_url())


class DCList(generic.ListView):
    model = models.DataCenter
    template_name = 'core/settings/data_center/list.html'
    paginate_by = 20

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user)


class DCUpdate(generic.UpdateView):
    form_class = forms.DataCenter
    model = models.DataCenter
    template_name = 'core/settings/data_center/edit.html'
    success_url = reverse_lazy('dc_list')

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user)


class DCDelete(generic.DeleteView):
    model = models.DataCenter
    template_name = 'core/settings/data_center/delete.html'
    success_url = reverse_lazy('dc_list')

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user)
