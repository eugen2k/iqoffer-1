# Generated by Django 2.1.2 on 2019-02-01 11:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0030_tariff_currency'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tariff',
            name='description',
            field=models.TextField(max_length=400),
        ),
    ]
