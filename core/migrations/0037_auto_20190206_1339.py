# Generated by Django 2.1.2 on 2019-02-06 13:39

import ckeditor_uploader.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0036_auto_20190206_1336'),
    ]

    operations = [
        migrations.AlterField(
            model_name='new',
            name='text',
            field=ckeditor_uploader.fields.RichTextUploadingField(),
        ),
    ]
