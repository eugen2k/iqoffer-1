# -*- coding:utf-8 -*-
from __future__ import unicode_literals

from django.template import Library
from core import models
import datetime

register = Library()


@register.filter
def get_user_published_orders(orders):
    return orders.filter(was_published=True, offer__isnull=True)


@register.filter
def startswith(text, starts):
    return text.startswith(starts)


@register.filter
def is_winner(orders, user):
    if not user.is_authenticated:
        return False
    if orders.filter(winner=user):
        return True
    return False


@register.simple_tag()
def can_respond_order(user, order):
    if user.is_authenticated:
        service_subs = [service.subcategory for service in models.UserService.objects.filter(user=user)]
        if order.subcategory in service_subs:
            return True
    return False


@register.simple_tag()
def messages_can_see_contacts(user, order):
    return user == order.owner or (
                not order.show_contacts_only_for_winner and is_winner(order.owner.my_orders.all(), user))


@register.simple_tag()
def get_offer(user, order):
    try:
        return models.Offer.objects.filter(order=order, user=user).latest('created_at')
    except Exception as e:
        return None


@register.simple_tag()
def get_offer_for_messages(user, order):
    try:
        return models.Offer.objects.filter(order=order, user=user, is_declined=False).latest('created_at')
    except Exception as e:
        return None


@register.simple_tag()
def get_customer_service_new_messages(service, customer):
    return models.Message.objects.filter(service=service, is_read=False, recipient=customer)


@register.simple_tag()
def get_order_new_messages(user, order):
    return models.Message.objects.filter(order=order, is_read=False, recipient=user)
