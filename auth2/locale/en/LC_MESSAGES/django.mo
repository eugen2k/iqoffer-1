��          D      l       �      �   $   �      �      �   B    �   F  u   �  7   G  -                             Phrase:LoginTryNotActiveUser Phrase:RegistrationActivationMessage Phrase:RetrySendActivationMail Phrase:SuccessActivation Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Please confirm your email address. If you did not receive the activation code, please use the link 'Does not the activation code come up?' A link for confirming the address has been sent to your email. After confirmation, you can use your personal account. Your activation code has been sent to your email again. Your account has been successfully activated. 