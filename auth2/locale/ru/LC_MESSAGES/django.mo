��            )   �      �     �  &   �     �  %   �       4        B     X     _     h     �     �     �     �  $   �          %     >     G     [     l     r     �     �     �     �  #   �  0   �      	  �  *     �  y        �  ,   �  !   �  ^   �     L  
   g     r  +     '   �     �  $   �  \   	  �   m	  �   f
  <   �
     6  1   I  %   {     �  *   �  *   �  
   
  $     *   :  O   e  f   �  5        	                                                                                         
                                           Change password Check your email for password recovery Customer Does not the activation code come up? Email Enter the same password as before, for verification. Forgot your password? Log in Password Password changed successfully. Password confirmation Password reset Passwords doesn`t match Phrase:LoginTryNotActiveUser Phrase:RegistrationActivationMessage Phrase:RetrySendActivationMail Phrase:SuccessActivation Provider Registration accept Restore password Retry Save new password Set new password Sign in Sign up User not found User not found or already activated You must be logged in to view the requested page e.g. nickname or last/first name Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Сменить пароль Проверьте указанную электронную почту для восстановления пароля. Заказчик Не пришел код активации? Электронная почта Введите такой же пароль, как раньше для верификации Забыли пароль? Войти Пароль Пароль успешно изменён. Подтверждение пароля Сброс пароля Пароли не совпадают Пользователь не подтвердил свою элекронную почту. Информация о регистрации направлена на указанную электронную почту. После подтверждения электронной почты можно использовать сервис. Ссылка для подтверждения была отправлена на указанный адрес электронной почты. Ваш аккаунт успешно активирован. Провайдер Подтверждение регистрации Восстановить пароль Повторить Сохранить новый пароль Сохранить новый пароль Войти Зарегистрироваться Пользователь не найден Пользователь не найден или уже активирован Вы должны войти в систему для просмотра данной страницы например, ник или фамилия/имя 