# -*- coding: utf-8 -*-
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.http import force_bytes, urlsafe_base64_encode
from auth2.tasks import send_mail
from auth2.tokens import password_reset_token_generator, activate_user_token_generator
from django.utils.translation import ugettext_lazy as _


def send_reset_password_mail(request, user):
    uidb64 = urlsafe_base64_encode(force_bytes(user.pk)).decode("utf-8")
    token = password_reset_token_generator.make_token(user)
    host = request.get_host()
    url = reverse('set_password', args=[uidb64, token])

    password_reset_template = render_to_string('core/emails/reset_password.%s.html' %
                                               request.LANGUAGE_CODE, {'link': 'https://' + host + url})
    send_mail(_('Password reset'), '', [user.email], html_message=password_reset_template)


def send_activation_mail(request, user):
    uidb64 = urlsafe_base64_encode(force_bytes(user.pk)).decode("utf-8")
    token = activate_user_token_generator.make_token(user)
    host = request.get_host()
    url = reverse('activate', args=[uidb64, token])

    registration_template = render_to_string('core/emails/registration.%s.html' %
                                             request.LANGUAGE_CODE, {'link': 'https://' + host + url})
    send_mail(_('Registration accept'), '', [user.email], html_message=registration_template)
