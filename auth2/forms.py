# -*- coding:utf-8 -*-
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django import forms
from django.utils.translation import ugettext_lazy as _

from django.contrib.auth import get_user_model
from core.forms import TranslateMixin
from core import models

User = get_user_model()


class UserCreateForm(forms.ModelForm):
    user_type = forms.ChoiceField(choices=[('customer', _('Customer')), ('provider', _('Provider'))])
    password1 = forms.CharField(label=_("Password"),
                                strip=False,
                                widget=forms.PasswordInput)
    password2 = forms.CharField(label=_("Password confirmation"),
                                widget=forms.PasswordInput,
                                strip=False,
                                help_text=_("Enter the same password as before, for verification."))

    class Meta:
        model = User
        fields = ('user_type', "email", 'username', 'country',)

    def __init__(self, *args, **kwargs):
        initial = kwargs.get('initial')
        super().__init__(*args, **kwargs)
        if not initial:
            pass
        elif initial.get('email'):
            self.fields['email'].widget.attrs['readonly'] = True

        self.fields['username'].help_text = _('e.g. nickname or last/first name')
        self.fields['country'].choices = [['', _('Country')]] + [[c.id, _(c.name)] for c in
                                                                 models.Country.objects.all()]

    def clean_email(self):
        return self.cleaned_data['email'].lower().strip()

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                _('Passwords doesn`t match'),
                code='password_mismatch',
            )
        return password2

    def save(self, commit=True):
        user = super(UserCreateForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user_type = self.cleaned_data.pop('user_type')
        if user_type == 'provider':
            user.is_provider = True
        if user_type == 'customer':
            user.is_customer = True
        if commit:
            user.save()
        return user


class LoginForm(AuthenticationForm):
    # username = forms.EmailField(label='Электронная почта')

    def clean(self):
        username = self.cleaned_data.get('username', '').strip().lower()
        password = self.cleaned_data.get('password')

        if username and password:
            self.user_cache = authenticate(username=username, password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'username': self.username_field.verbose_name},
                )

        return self.cleaned_data


class PasswordResetForm(forms.Form):
    email = forms.EmailField(label=_('Email'))

    def clean_email(self):
        email = self.cleaned_data['email'].strip()
        if not User.objects.filter(email=email).exists():
            raise forms.ValidationError(_('User not found'))
        return email


class RetryActivateForm(PasswordResetForm):
    def clean_email(self):
        email = self.cleaned_data['email'].strip()
        if not User.objects.filter(email=email, is_active=False).exists():
            raise forms.ValidationError(_('User not found or already activated'))
        return email
