from __future__ import unicode_literals

from django.contrib import messages
from django.http import Http404
from django.urls import reverse, reverse_lazy
from django.utils.encoding import force_text

from auth2.mails import send_reset_password_mail, send_activation_mail
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _

from django.utils.http import *
from django.views.decorators.debug import sensitive_post_parameters
from django.views.decorators.cache import never_cache
from django.contrib.auth.forms import SetPasswordForm

from django.contrib.auth.views import auth_login, auth_logout, PasswordChangeView

from django.shortcuts import redirect, render

from auth2.forms import UserCreateForm, LoginForm, PasswordResetForm, RetryActivateForm
from auth2.tokens import password_reset_token_generator, activate_user_token_generator
from core import models
from django.forms.models import model_to_dict
import auth2.translate

User = get_user_model()


@sensitive_post_parameters()
@never_cache
def login(request):
    request.session.set_test_cookie()

    get_params = '?'
    for key, val in request.GET.items():
        if key != 'next':
            get_params += key + '=' + val + '&'
    redirect_to = request.GET.get('next', reverse('index')) + get_params

    form = LoginForm(data=request.POST or None, request=request)

    if form.is_valid():

        if form.user_cache is None or form.user_cache.is_active is False:
            messages.info(request, auth2.translate.LOGIN_NOT_ACTIVE_USER)
            return redirect(reverse('login'))

        auth_login(request, form.get_user())

        if request.session.test_cookie_worked():
            request.session.delete_test_cookie()

        return redirect(redirect_to)

    context = {
        'title': _('Sign in'),
        'form': form,
        'next': redirect_to,
    }
    return render(request, 'auth2/login.html', context)


@sensitive_post_parameters()
@never_cache
def registration(request):
    pre_user = None
    user_data = dict()

    request.session.set_test_cookie()

    invite_token = request.GET.get('invite')

    if invite_token:
        pre_user = models.PreUser.objects.get(token=invite_token)
        user_data = model_to_dict(pre_user)
        user_type = None
        if user_data['is_customer']:
            user_type = 'customer'
        elif user_data['is_provider']:
            user_type = 'provider'
        user_data.update({'user_type': user_type})
    elif request.GET.get('user_type'):
        user_data.update({'user_type': request.GET.get('user_type')})

    elif request.GET.get('provider'):
        user_data.update({'user_type': 'provider'})

    elif request.GET.get('customer'):
        user_data.update({'user_type': 'customer'})

    form = UserCreateForm(request.POST or None, initial=user_data)

    if form.is_valid():
        user = form.save(commit=False)
        if pre_user:
            user.avatar = pre_user.avatar
        user.save()

        if pre_user:
            pre_user.user = user
            models.PreUser.objects.filter(email=pre_user.email).exclude(id=pre_user.id).update(show_in_stat=False)
            pre_user.save()
            settings = dict(
                office=models.Office.objects.filter(pre_user=pre_user, user__isnull=True),
                service=models.UserService.objects.filter(pre_user=pre_user, user__isnull=True),
                contacts=models.UserContact.objects.filter(pre_user=pre_user, user__isnull=True),
                data_center=models.DataCenter.objects.filter(pre_user=pre_user, user__isnull=True),
                requisites=models.Requisites.objects.filter(pre_user=pre_user, user__isnull=True)
            )
            for key, objects in settings.items():
                for obj in objects:
                    obj.user = user
                    obj.save()

        send_activation_mail(request, user)

        messages.success(
            request,
            auth2.translate.REGISTRATION_ACTIVATION_MESSAGE
        )

        get_params = '?'
        for key, val in request.GET.items():
            get_params += key + '=' + val + '&'

        redirect_to = reverse('login')
        if get_params != '?':
            redirect_to += get_params

        return redirect(redirect_to)

    context = {
        'title': _('Sign up'),
        'form': form,
    }
    return render(request, 'auth2/registration.html', context)


def retry(request):
    form = RetryActivateForm(request.POST or None)
    if form.is_valid():
        email = form.cleaned_data['email']
        user = User.objects.get(email=email)
        send_activation_mail(request, user)
        messages.info(request, auth2.translate.RETRY_SEND_ACTIVATION_MAIL)

    context = {
        'title': _('Does not the activation code come up?'),
        'form': form
    }
    return render(request, 'auth2/retry.html', context)


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
        assert activate_user_token_generator.check_token(user, token)
    except Exception as e:
        raise Http404

    user.is_active = True
    user.save()
    messages.success(request, auth2.translate.SUCCESS_ACTIVATION)
    return redirect(reverse('login'))


def set_password(request, uidb64, token):
    uid = force_text(urlsafe_base64_decode(uidb64))
    user = User.objects.get(pk=uid)
    assert password_reset_token_generator.check_token(user, token)

    form = SetPasswordForm(user, request.POST or None)
    if form.is_valid():
        form.save()
        return redirect(reverse('login'))

    context = {
        'title': _('Set new password'),
        'form': form
    }

    return render(request, 'auth2/password_set.html', context)


def logout(request):
    auth_logout(request)
    return redirect(reverse('login'))


def password_reset(request):
    form = PasswordResetForm(request.POST or None)
    if form.is_valid():
        email = form.cleaned_data['email']
        user = User.objects.get(email=email)
        send_reset_password_mail(request, user)
        messages.info(request, _('Check your email for password recovery'))

    context = {
        'title': _('Password reset'),
        'form': form
    }
    return render(request, 'auth2/password_reset_form.html', context)


class PasswordChange(PasswordChangeView):
    success_url = reverse_lazy('index')
    template_name = 'auth2/password_change.html'

    def form_valid(self, form):
        res = super().form_valid(form)
        messages.success(self.request, _('Password changed successfully.'))
        return res


password_change = PasswordChange.as_view()
