# coding: utf-8
from django.conf.urls import url

from auth2 import views


urlpatterns = [
    url(r'^login/$', views.login, name='login'),
    url(r'^registration/$', views.registration, name='registration'),

    url(r'^logout/$', views.logout, name='logout'),
    url(r'^activate/(?P<uidb64>\w+)/(?P<token>\w+-\w+)/$', views.activate, name='activate'),
    url(r'^retry/$', views.retry, name='retry'),
    url(r'^password/reset/$', views.password_reset, name='password_reset'),
    url(r'^password/set/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.set_password, name='set_password'),

    url(r'^password/change/$', views.password_change, name='password_change'),
]
