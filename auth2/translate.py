from django.utils.translation import ugettext_lazy as _

LOGIN_NOT_ACTIVE_USER = _('Phrase:LoginTryNotActiveUser')
# 'Please confirm your email address. If you did not receive the activation code, please use the link "Does not the activation code come up?"'

REGISTRATION_ACTIVATION_MESSAGE = _('Phrase:RegistrationActivationMessage')
# _('A link for confirming the address has been sent to your email. After confirmation, you can use your personal account.')

RETRY_SEND_ACTIVATION_MAIL = _('Phrase:RetrySendActivationMail')
# _('Your activation code has been sent to your email again.')

SUCCESS_ACTIVATION = _('Phrase:SuccessActivation')
# _('Your account has been successfully activated.')

