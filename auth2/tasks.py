# -*- coding: utf-8 -*-
from django.conf import settings
from django.core.mail import send_mail as django_send_mail


def send_mail(subject, body, to_mails, **kwargs):
    django_send_mail(subject, body, settings.DEFAULT_FROM_EMAIL, to_mails, **kwargs)
