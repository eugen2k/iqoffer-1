# -*- coding:utf-8 -*-
from __future__ import unicode_literals

from django.contrib import messages
from django.shortcuts import redirect
from django.urls import reverse, resolve
from django.utils.translation import ugettext_lazy as _


class LoginRequired:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        user = request.user

        available_urls = ('order_list', 'order_detail', 'full_service_list',
                          'service', 'profile', 'change_language', 'registration',
                          'login', 'index', 'activate', 'retry', 'password_reset',
                          'set_password', 'password_change', 'privacy', 'terms', 'faq')

        available_path = ('/ajax', '/media', '/static', '/ru/ajax', '/__debug__/', '/news')

        if not user.is_authenticated and not resolve(
                request.path).url_name in available_urls and not request.path.startswith(available_path):
            messages.add_message(request, messages.WARNING, _('You must be logged in to view the requested page'))
            return redirect(reverse('login') + '?next=%s' % request.get_full_path())

        response = self.get_response(request)
        return response
