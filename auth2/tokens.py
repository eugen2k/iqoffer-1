from django.contrib.auth.tokens import PasswordResetTokenGenerator as DefaultTokenGenerator


class PasswordResetTokenGenerator(DefaultTokenGenerator):
    key_salt = '3noacri3joiow3r3'
password_reset_token_generator = PasswordResetTokenGenerator()


class ActivateUserTokenGenerator(DefaultTokenGenerator):
    key_salt = 'nmrco3n2orico3e23c'
activate_user_token_generator = ActivateUserTokenGenerator()
